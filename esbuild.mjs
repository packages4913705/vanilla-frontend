import esbuild from "esbuild"
import { sassPlugin } from "esbuild-sass-plugin"
import postcss from 'postcss'
import autoprefixer from 'autoprefixer'

esbuild
    .build({
        entryPoints: ["scss/vanilla-frontend.scss"],
        entryNames: '[name]',
        outbase: "./",
        outdir: './bundles',
        bundle: true,
        metafile: true,
        minify: true,
        plugins: [
            sassPlugin({
                async transform(source) {
                    const { css } = await postcss([autoprefixer]).process(source)
                    return css
                },
            }),
        ],
    })
    .then(() => console.log("⚡ Build complete! ⚡"))
    .catch(() => process.exit(1))
