/**
 * ------------------------------------------------------------------
 * TEST for the _consent.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from Cookie - Has all the public method
 * - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init(): Make sure that the dialog is open if the cookie doesn't exist
 * - #Init(): Emmit the dialog:closed event will run the set() method
 * - #Init(): Emmit the dialog:submiting event will run the set() method and save the inputs value
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import Dialog from "../_dialog"
import Consent from "../_consent"
import ErrorMessage from "../utilities/_error"

let fakeCookie, fakeCookiePref, fakeDefaultCookie

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<dialog id="cookies"></dialog>' +
        '<dialog id="cookiesPreferences" aria-modal="true">' +
        '<form method="dialog">' +
        '<input type="checkbox" name="cookies_consent[]" value="necessary" checked disabled>' +
        '<input type="checkbox" name="cookies_consent[]" value="analytic" checked>' +
        '<input type="checkbox" name="cookies_consent[]" value="marketing" >' +
        '</form>' +
        '</dialog>'

    fakeCookie = new Consent('_consent', 'cookies')
    fakeCookiePref = new Consent('_consent', 'cookiesPreferences')
    fakeDefaultCookie = new Consent()

})

describe('Structure of the class', () => {

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Consent('test', {})).toThrowError(ErrorMessage.typeOf('dialogID', 'string'))
        expect(() => new Consent('test', 'make-error')).toThrowError(ErrorMessage.existById('dialog', 'make-error'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeCookie).toHaveProperty('_dialog')
        expect(fakeCookie._dialog).toBeInstanceOf(Dialog)
        expect(fakeCookie._dialog._element).toStrictEqual(document.getElementById('cookies'))
        expect(fakeCookie.value).toStrictEqual({})
        expect(fakeDefaultCookie._dialog).toBeInstanceOf(Dialog)
    })

})

describe('#Init()', () => {

    test('Make sure that the dialog is open if the cookie doesn\'t exist', () => {
        expect(fakeCookie.value).toStrictEqual({})
        expect(fakeCookie._dialog.open).toBeTruthy()
    })

    test('Emmit the dialog:closed event will run the set() method and save the inputs value', () => {
        const methodSpy = vi.spyOn(fakeCookie, 'set')
        expect(methodSpy).not.toHaveBeenCalled()
        fakeCookie._dialog.close()
        expect(methodSpy).toHaveBeenCalled()
        expect(fakeCookie.value).toStrictEqual({ necessary: true })
    })

    test('Emmit the dialog:submiting event will run the set() method and save the inputs value', () => {
        const methodSpy = vi.spyOn(fakeCookiePref, 'set')
        expect(methodSpy).not.toHaveBeenCalled()
        fakeCookiePref._dialog.submit()
        expect(methodSpy).toHaveBeenCalled()
        expect(fakeCookie.value).toStrictEqual({ necessary: true, analytic: true, marketing: false })
    })

})