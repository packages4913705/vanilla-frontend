/**
 * ------------------------------------------------------------------
 * TEST for the _check-all.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init(): Toggle checkboxes on change the _element
 * - #Init(): Check all the checkboxes will set the :checked ont the _element 
 * - #Init(): Uncheck one of the checkboxes will remove the :checked on the _element
 */

import { describe, test, expect, beforeAll } from "vitest"
import { fireEvent } from "@testing-library/dom"
import CheckAll from "../_check-all"

let fakeCheckbox

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<input id="fakeCheckbox" type="checkbox" name="all" value="fake[]">' +
        '<input id="one" type="checkbox" name="fake[]" value="1" checked>' +
        '<input id="two" type="checkbox" name="fake[]" value="2">' +
        '<input id="three" type="checkbox" name="fake[]" value="3">'

    fakeCheckbox = new CheckAll(document.getElementById('fakeCheckbox'))

})

describe('Structure of the class', () => {

    test('Return the correct properties', () => {
        expect(fakeCheckbox._checkboxes).toBeTypeOf('object')
        expect(fakeCheckbox._checkboxes).toStrictEqual(document.querySelectorAll('input[type="checkbox"][name="fake[]"]'))
        expect(fakeCheckbox.number).toBeInstanceOf(Object)
        expect(fakeCheckbox.number).toStrictEqual({
            total: 3,
            checked: 1,
            unchecked: 2
        })
    })

})

describe('#Init()', () => {

    test('Toggle checkboxes on change the _element', () => {
        expect(fakeCheckbox._element.checked).toBeFalsy()
        fireEvent.change(fakeCheckbox._element, { target: { checked: true } })
        expect(fakeCheckbox._element.checked).toBeTruthy()
        expect(fakeCheckbox.number.checked).toBe(3)
        expect(fakeCheckbox.number.unchecked).toBe(0)
        fireEvent.change(fakeCheckbox._element, { target: { checked: false } })
        expect(fakeCheckbox._element.checked).toBeFalsy()
        expect(fakeCheckbox.number.checked).toBe(0)
        expect(fakeCheckbox.number.unchecked).toBe(3)
    })

    test('Check all the checkboxes will set the :checked ont the _element', () => {
        const one = document.getElementById('one')
        const two = document.getElementById('two')
        const three = document.getElementById('three')
        expect(fakeCheckbox._element.checked).toBeFalsy()
        fireEvent.change(one, { target: { checked: true } })
        expect(fakeCheckbox._element.checked).toBeFalsy()
        expect(fakeCheckbox.number.checked).toBe(1)
        fireEvent.change(two, { target: { checked: true } })
        expect(fakeCheckbox._element.checked).toBeFalsy()
        expect(fakeCheckbox.number.checked).toBe(2)
        fireEvent.change(three, { target: { checked: true } })
        expect(fakeCheckbox._element.checked).toBeTruthy()
        expect(fakeCheckbox.number.checked).toBe(3)
    })

    test('Uncheck one of the checkboxes will remove the :checked on the _element', () => {
        const one = document.getElementById('one')
        fireEvent.change(one, { target: { checked: false } })
        expect(fakeCheckbox._element.checked).toBeFalsy()
        expect(fakeCheckbox.number.checked).toBe(2)
    })

})
