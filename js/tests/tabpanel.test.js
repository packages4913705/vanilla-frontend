/**
 * ------------------------------------------------------------------
 * TEST for the_tabpabel.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Change(): Toggle the [aria-selected] attribute on the tab
 * - #Change(): Toggle the [hidden] attribute on the panel
 * - #Change(): Emmit the tabpanel:changed event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Tabpanel from "../_tabpanel"
import ErrorMessage from "../utilities/_error"

let fakeTabpanel, fakeTabA, fakeTabB, fakeTabC, fakePanelA, fakePanelB

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<div id="tabpanel">' +
        '<div role="tablist">' +
        '<button role="tab" aria-controls="a" aria-selected="false">A</button>' +
        '<button role="tab" aria-controls="b" aria-selected="false">B</button>' +
        '<button role="tab" aria-selected="false">C</button>' +
        '</div>' +
        '<div role="tabpanel" id="a" hidden>AAA</div>' +
        '<div role="tabpanel" id="b" hidden>BBB</div>' +
        '<div role="tabpanel" id="c" hidden>CCC</div>' +
        '</div>'

    fakeTabpanel = new Tabpanel(document.getElementById('tabpanel'))
    fakeTabA = fakeTabpanel._tabs[0]
    fakeTabB = fakeTabpanel._tabs[1]
    fakeTabC = fakeTabpanel._tabs[2]
    fakePanelA = fakeTabpanel._panels[0]
    fakePanelB = fakeTabpanel._panels[1]
})

describe('Structure of the class', () => {

    test('Constructor: Passing the correct parameters', () => {
        const fakeWrong = document.createElement('div')
        expect(() => new Tabpanel(fakeWrong)).toThrowError(ErrorMessage.exist('button [role="tab"]'))
        const fakeTab = document.createElement('button')
        fakeTab.setAttribute('role', 'tab')
        fakeWrong.appendChild(fakeTab)
        expect(() => new Tabpanel(fakeWrong)).toThrowError(ErrorMessage.exist('div [role="tabpanel"]'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeTabpanel).toHaveProperty('_tabs')
        expect(fakeTabpanel).toHaveProperty('_panels')
        expect(fakeTabpanel).toHaveProperty('_current')
        expect(fakeTabpanel._tabs).toBeTypeOf('object')
        expect(fakeTabpanel._panels).toBeTypeOf('object')
        expect(fakeTabpanel._current).toBeTypeOf('object')
        expect(fakeTabpanel._tabs).toStrictEqual(document.querySelectorAll('#tabpanel [role="tab"]'))
        expect(fakeTabpanel._panels).toStrictEqual(document.querySelectorAll('#tabpanel [role="tabpanel"]'))
        expect(fakeTabpanel._current).toStrictEqual(document.querySelector('#tabpanel button'))
    })

})

describe('#Change()', () => {

    test('Toggle the [aria-selected] attribute on the tab', () => {
        expect(fakeTabA.getAttribute('aria-selected')).toBe('true')
        expect(fakeTabB.getAttribute('aria-selected')).toBe('false')
        fireEvent(fakeTabB, new MouseEvent('click'))
        expect(fakeTabA.getAttribute('aria-selected')).toBe('false')
        expect(fakeTabB.getAttribute('aria-selected')).toBe('true')
        fireEvent(fakeTabC, new MouseEvent('click'))
        expect(fakeTabA.getAttribute('aria-selected')).toBe('false')
        expect(fakeTabB.getAttribute('aria-selected')).toBe('true')
        fireEvent(fakeTabA, new MouseEvent('click'))
        expect(fakeTabA.getAttribute('aria-selected')).toBe('true')
        expect(fakeTabB.getAttribute('aria-selected')).toBe('false')
    })

    test('Toggle the [hidden] attribute on the panel', () => {
        expect(fakePanelA.hidden).toBeFalsy()
        expect(fakePanelB.hidden).toBeTruthy()
        fireEvent(fakeTabB, new MouseEvent('click'))
        expect(fakePanelA.hidden).toBeTruthy()
        expect(fakePanelB.hidden).toBeFalsy()
        fireEvent(fakeTabC, new MouseEvent('click'))
        expect(fakePanelA.hidden).toBeTruthy()
        expect(fakePanelB.hidden).toBeFalsy()
        fireEvent(fakeTabA, new MouseEvent('click'))
        expect(fakePanelA.hidden).toBeFalsy()
        expect(fakePanelB.hidden).toBeTruthy()
    })

    test('Emmit the tabpanel:changed event', () => {
        const eventSpy = vi.spyOn(fakeTabpanel, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeTabA, new MouseEvent('click'))
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeTabC, new MouseEvent('click'))
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeTabB, new MouseEvent('click'))
        expect(eventSpy).toHaveBeenCalledWith('changed', { current: fakeTabB })
    })

})
