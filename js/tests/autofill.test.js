/**
 * ------------------------------------------------------------------
 * TEST for the _autofill.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init(): Change <select> will set a value in another input
 * - #Init(): Change <input> will set a value in another input
 * - #Init(): Change <input type="file"> will set a value in another input
 * - #ByOption(): Emmit the autofill:changed event
 * - #byFile(): Emmit the autofill:changed event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Autofill from "../_autofill"
import ErrorMessage from "../utilities/_error"

let fakeSelect, fakeList, fakeFile, fakeAutofillSelect, fakeAutofillList, fakeAutofillFile
/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<select id="fakeSelect" aria-controls="fakeSelectInput">' +
        '<option value="null">--</option>' +
        '<option value="cat" data-id="1">Cat</option>' +
        '</select>' +
        '<input id="fakeSelectInput" type="text" data-autofill="id">' +

        '<input id="fakeList" list="fakeDataList" aria-controls="fakeListInput">' +
        '<datalist id="fakeDataList">' +
        '<option data-id="1" value="cat">' +
        '<option data-id="2" value="dog">' +
        '</datalist>' +
        '<input id="fakeListInput" type="text" data-autofill="id">' +

        '<input id="fakeFile" type="file" aria-controls="fakeFileInput fakeFileName fakeFileExt">' +
        '<input id="fakeFileInput" type="text" data-autofill="name" >' +
        '<input id="fakeFileName" type="text" data-autofill="filename">' +
        '<input id="fakeFileExt" type="text" data-autofill="extension">'

    fakeSelect = document.getElementById('fakeSelect')
    fakeList = document.getElementById('fakeList')
    fakeFile = document.getElementById('fakeFile')

    fakeAutofillSelect = new Autofill(fakeSelect)
    fakeAutofillList = new Autofill(fakeList)
    fakeAutofillFile = new Autofill(fakeFile)

})

describe('Structure of the class', () => {

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Autofill('make-error')).toThrowError(ErrorMessage.instanceOf('el', 'HTMLElement'))
        expect(() => new Autofill(document.createElement('button'))).toThrowError(ErrorMessage.typeOf('el', 'input|select'))
        expect(() => new Autofill(document.createElement('select'))).toThrowError(ErrorMessage.withAttribute('el', 'aria-controls'))
        const fakeWrongInput = document.createElement('input')
        fakeWrongInput.setAttribute('aria-controls', 'something')
        fakeWrongInput.type = 'checkbox'
        expect(() => new Autofill(fakeWrongInput)).toThrowError(ErrorMessage.typeOf('type', 'text|file'))
        fakeWrongInput.type = 'text'
        expect(() => new Autofill(fakeWrongInput)).toThrowError(ErrorMessage.withAttribute('input', 'list'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeAutofillSelect).toHaveProperty('_fields')
        expect(fakeAutofillSelect).toHaveProperty('_type')
        expect(fakeAutofillSelect._fields).toBeTypeOf('object')
        expect(fakeAutofillSelect._fields).toStrictEqual([...document.querySelectorAll('#fakeSelectInput')])
        expect(fakeAutofillSelect._type).toBe('select')
        expect(fakeAutofillList._fields).toBeTypeOf('object')
        expect(fakeAutofillList._fields).toStrictEqual([...document.querySelectorAll('#fakeListInput')])
        expect(fakeAutofillList._type).toBe('datalist')
        expect(fakeAutofillFile._fields).toBeTypeOf('object')
        expect(fakeAutofillFile._fields).toStrictEqual([...document.querySelectorAll('#fakeFileInput,#fakeFileName,#fakeFileExt')])
        expect(fakeAutofillFile._type).toBe('file')
    })

})

describe('#Init()', () => {

    test('Change <select> will set a value in another input', () => {
        const fakeInput = document.getElementById('fakeSelectInput')
        expect(fakeSelect.value).toBe('null')
        expect(fakeInput.value).toBe('')
        fireEvent.change(fakeSelect, { target: { value: 'cat' } })
        expect(fakeSelect.value).toBe('cat')
        expect(fakeInput.value).toBe('1')
        fireEvent.change(fakeSelect, { target: { value: 'null' } })
        expect(fakeSelect.value).toBe('null')
        expect(fakeInput.value).toBe('')
    })

    test('Change <input> will set a value in another input', () => {
        const fakeInput = document.getElementById('fakeListInput')
        expect(fakeList.value).toBe('')
        expect(fakeInput.value).toBe('')
        fireEvent.change(fakeList, { target: { value: 'dog' } })
        expect(fakeList.value).toBe('dog')
        expect(fakeInput.value).toBe('2')
        fireEvent.change(fakeList, { target: { value: 'cat' } })
        expect(fakeList.value).toBe('cat')
        expect(fakeInput.value).toBe('1')
        fireEvent.change(fakeList, { target: { value: '' } })
        expect(fakeList.value).toBe('')
        expect(fakeInput.value).toBe('')
    })

    test('Change <input type="file"> will set a value in another input', () => {
        const fakeFileInput = document.getElementById('fakeFileInput')
        const fakeFileName = document.getElementById('fakeFileName')
        const fakeFileExt = document.getElementById('fakeFileExt')
        const fakeImage = new File(["my testing file"], "my-file.jpg", {
            type: "image/jpeg",
        })
        expect(fakeFileInput.value).toBe('')
        expect(fakeFileName.value).toBe('')
        expect(fakeFileExt.value).toBe('')
        fireEvent.change(fakeFile, { target: { files: [fakeImage] } })
        expect(fakeFileInput.value).toBe('my-file.jpg')
        expect(fakeFileName.value).toBe('my-file')
        expect(fakeFileExt.value).toBe('jpg')
    })

})

describe('#ByOption()', () => {

    test('Emmit the autofill:changed event', () => {
        const eventSpy = vi.spyOn(fakeAutofillSelect, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent.change(fakeSelect, { target: { value: 'cat' } })
        expect(eventSpy).toHaveBeenCalledWith('changed', { current: document.querySelector('option[value="cat"]') })
    })

})

describe('#ByFile()', () => {

    test('Emmit the autofill:changed event', () => {
        const fakeImage = new File(["my testing file"], "my-file.jpg", {
            type: "image/jpeg",
        })
        const eventSpy = vi.spyOn(fakeAutofillFile, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent.change(fakeFile, { target: { files: [fakeImage] } })
        expect(eventSpy).toHaveBeenCalledWith('changed', { current: fakeImage })
    })

})