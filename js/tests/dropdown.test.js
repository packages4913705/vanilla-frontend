/**
 * ------------------------------------------------------------------
 * TEST for the _dropdown.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - Constructor: Constructor: Passing the correct parameters
 * - #Init(): Close content if click outside
 * - #Toggle(): Toggle the [aria-pressed] and [aria-expanded] attribute on the <button>
 * - #Toggle(): Toggle the [hidden] attribute on the <div>
 * - #Toggle(): Emmit the toggle:changed event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Dropdown from "../_dropdown"
import ErrorMessage from "../utilities/_error"

let fakeDiv, fakeButton, fakeContent, fakeDropdown, fakeDropdownClosable

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<div id="fakeDropdown">' +
        '<button id="fakeButton" aria-controls="fakeContent" aria-expanded="false" aria-pressed="false"></button>' +
        '<div id="fakeContent" tabindex="0" hidden></div>' +
        '</div>' +
        '<div id="fakeDropdownClosable">' +
        '<button aria-controls="fakeContentClosable" aria-expanded="false" aria-pressed="false"></button>' +
        '<div id="fakeContentClosable" tabindex="0" hidden></div>' +
        '</div>'

    fakeDiv = document.getElementById('fakeDropdown')
    fakeButton = document.getElementById('fakeButton')
    fakeContent = document.getElementById('fakeContent')

    fakeDropdown = new Dropdown(fakeDiv, {
        closable: false
    })
    fakeDropdownClosable = new Dropdown(document.getElementById('fakeDropdownClosable'), {
        closable: true
    })
})

describe('Structure of the class', () => {

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Dropdown(fakeDiv, { closable: 'make-error' })).toThrowError(ErrorMessage.typeOf('options.closable', 'boolean'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeDropdown).toHaveProperty('_button')
        expect(fakeDropdown).toHaveProperty('_content')
        expect(fakeDropdown._button).toBe(fakeButton)
        expect(fakeDropdown._content).toBe(fakeContent)
    })

})

describe('#Init()', () => {

    test('Close content if click outside', () => {
        expect(fakeDropdownClosable._button.getAttribute('aria-pressed')).toBe('false')
        expect(fakeDropdownClosable._button.getAttribute('aria-expanded')).toBe('false')
        expect(fakeDropdownClosable._content.hidden).toBeTruthy()
        fireEvent(fakeDropdownClosable._button, new MouseEvent('click'))
        expect(fakeDropdownClosable._button.getAttribute('aria-pressed')).toBe('true')
        expect(fakeDropdownClosable._button.getAttribute('aria-expanded')).toBe('true')
        expect(fakeDropdownClosable._content.hidden).toBeFalsy()
        fireEvent(fakeDropdownClosable._content, new MouseEvent('focusout'))
        expect(fakeDropdownClosable._button.getAttribute('aria-expanded')).toBe('false')
        expect(fakeDropdownClosable._button.getAttribute('aria-pressed')).toBe('false')
        expect(fakeDropdownClosable._content.hidden).toBeTruthy()
    })

})


describe('#Toggle()', () => {

    test('Toggle the [aria-pressed] and [aria-expanded] attribute on the <button>', () => {
        expect(fakeDropdown._button.getAttribute('aria-pressed')).toBe('false')
        expect(fakeDropdown._button.getAttribute('aria-expanded')).toBe('false')
        fireEvent(fakeDropdown._button, new MouseEvent('click'))
        expect(fakeDropdown._button.getAttribute('aria-pressed')).toBe('true')
        expect(fakeDropdown._button.getAttribute('aria-expanded')).toBe('true')
        fireEvent(fakeDropdown._button, new MouseEvent('click'))
        expect(fakeDropdown._button.getAttribute('aria-pressed')).toBe('false')
        expect(fakeDropdown._button.getAttribute('aria-expanded')).toBe('false')
    })

    test('Toggle the [hidden] attribute on the <div>', () => {
        expect(fakeDropdown._content.hidden).toBeTruthy()
        fireEvent(fakeDropdown._button, new MouseEvent('click'))
        expect(fakeDropdown._content.hidden).toBeFalsy()
        fireEvent.blur(fakeDropdown._content)
        expect(fakeDropdown._content.hidden).toBeFalsy()
        fireEvent(fakeDropdown._button, new MouseEvent('click'))
        expect(fakeDropdown._content.hidden).toBeTruthy()
    })

    test('Emmit the dropdown:changed event', () => {
        const eventSpy = vi.spyOn(fakeDropdown, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeDropdown._button, new MouseEvent('click'))
        expect(eventSpy).toHaveBeenCalledWith('changed', { isOpen: true })
    })

})