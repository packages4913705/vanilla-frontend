/**
 * ------------------------------------------------------------------
 * TEST for the /utilities/_cookie.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - Set(): Passing the correct parameters 
 * - Set(): Save the new cookie value
 * - Has(): Passing the correct parameters 
 * - Has(): Verify the existance of a key in the cookie value
 * - Get(): Passing the correct parameters 
 * - Get(): Get value by key in the cookie
 * - Delete(): Delete the cookie
 */

import { describe, test, expect, beforeAll } from "vitest"
import Cookie from "../utilities/_cookie"
import ErrorMessage from "../utilities/_error"

let fakeCookie

/**
 * Before all tests
 * 
 */

beforeAll(() => {
    fakeCookie = new Cookie('monster')
})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeCookie.set).toBeTypeOf('function')
        expect(fakeCookie.has).toBeTypeOf('function')
        expect(fakeCookie.get).toBeTypeOf('function')
        expect(fakeCookie.delete).toBeTypeOf('function')
    })

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Cookie()).toThrowError(ErrorMessage.typeOf('name', 'string'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeCookie.value).toBeInstanceOf(Object)
        expect(fakeCookie.value).toStrictEqual({})
    })

})

describe('Set()', () => {

    test('Passing the correct parameters ', () => {
        expect(() => fakeCookie.set('make-error')).toThrowError(ErrorMessage.instanceOf('value', 'Object'))
    })

    test('Save the new cookie value', () => {
        fakeCookie.set({ 1: 'one' })
        expect(fakeCookie.value).toStrictEqual({ 1: 'one' })
        fakeCookie.set({ 1: 'one', 2: 'two' })
        expect(fakeCookie.value).toStrictEqual({ 1: 'one', 2: 'two' })
    })

})

describe('Has()', () => {

    test('Passing the correct parameters ', () => {
        expect(() => fakeCookie.has()).toThrowError(ErrorMessage.typeOf('key', 'string'))
    })

    test('Verify the existance of a key in the cookie value', () => {
        expect(fakeCookie.has('dont-exist')).toBeFalsy()
        expect(fakeCookie.has('1')).toBeTruthy()
    })

})

describe('Get()', () => {

    test('Passing the correct parameters ', () => {
        expect(() => fakeCookie.has()).toThrowError(ErrorMessage.typeOf('key', 'string'))
    })

    test('Get value by key in the cookie', () => {
        expect(fakeCookie.get('dont-exist')).toBeNull()
        expect(fakeCookie.get('1')).toBe('one')
    })

})

describe('Delete()', () => {

    test('Delete the cookie', () => {
        expect(fakeCookie.value).toStrictEqual({ 1: 'one', 2: 'two' })
        fakeCookie.delete()
        expect(fakeCookie.value).toStrictEqual({})
    })

})