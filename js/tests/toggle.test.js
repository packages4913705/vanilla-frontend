/**
 * ------------------------------------------------------------------
 * TEST for the _toggle.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Toggle(): Button : Toggle the [aria-pressed] and [aria-expanded] attribute on the <button>
 * - #Toggle(): Button : Toggle the [hidden] attribute on the <div>
 * - #Toggle(): Checkbox : Toggle the [aria-expanded] attribute on the <checkbox>
 * - #Toggle(): Checkbox : Toggle the [hidden] attribute on the <div>
 * - #Toggle(): Radio : Toggle the [aria-expanded] attribute on the <radio>
 * - #Toggle(): Radio : Toggle the [hidden] attribute on the <div>
 * - #Toggle(): Select : Toggle the [aria-expanded] attribute on the <select>
 * - #Toggle(): Select : Toggle the [hidden] attribute on the <div>
 * - #Toggle(): Emmit the toggle:changed event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Toggle from "../_toggle"

let fakeToggleButton, fakeToggleCheckbox, fakeToggleRadioA, fakeToggleRadioB, fakeToggleSelect

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<button id="fakeButton" aria-controls="fakeCollapse" aria-expanded="false" aria-pressed="false"></button>' +
        '<div id="fakeCollapse" hidden></div>' +
        '<input id="fakeCheckbox" type="checkbox" aria-controls="fakeCollapseCheckbox" aria-expanded="false">' +
        '<div id="fakeCollapseCheckbox" hidden></div>' +
        '<input id="fakeRadioA" type="radio" name="fake-radio" aria-controls="fakeCollapseRadioA" aria-expanded="false">' +
        '<input id="fakeRadioB" type="radio" name="fake-radio" aria-controls="fakeCollapseRadioB" aria-expanded="false">' +
        '<div id="fakeCollapseRadioA" hidden></div>' +
        '<div id="fakeCollapseRadioB" hidden></div>' +
        '<select id="fakeSelect" aria-controls="fakeCollapseSelectA fakeCollapseSelectB fakeCollapseSelectC" aria-expanded="false">' +
        '<option value="null"></option>' +
        '<option value="A"></option>' +
        '<optgroup label="G">' +
        '<option value="B"></option>' +
        '</optgroup>' +
        '</select>' +
        '<div id="fakeCollapseSelectA" data-toggle-when="A" hidden></div>' +
        '<div id="fakeCollapseSelectB" data-toggle-when="B" hidden></div>' +
        '<div id="fakeCollapseSelectG" data-toggle-when="G" hidden></div>'

    fakeToggleButton = new Toggle(document.getElementById('fakeButton'))
    fakeToggleCheckbox = new Toggle(document.getElementById('fakeCheckbox'))
    fakeToggleRadioA = new Toggle(document.getElementById('fakeRadioA'))
    fakeToggleRadioB = new Toggle(document.getElementById('fakeRadioB'))
    fakeToggleSelect = new Toggle(document.getElementById('fakeSelect'))

})

describe('Structure of the class', () => {

    test('Constructor: Return the correct properties', () => {
        expect(fakeToggleButton).toHaveProperty('_type')
        expect(fakeToggleButton).toHaveProperty('_toggables')
        expect(fakeToggleButton._type).toBe('button')
        expect(fakeToggleCheckbox._type).toBe('checkbox')
        expect(fakeToggleRadioA._type).toBe('radio')
        expect(fakeToggleSelect._type).toBe('select')
        expect(fakeToggleButton._toggables).toStrictEqual([...document.querySelectorAll('#fakeCollapse')])
        expect(fakeToggleSelect._toggables).toStrictEqual([...document.querySelectorAll('#fakeCollapseSelectA, #fakeCollapseSelectB, #fakeCollapseSelectC')])
    })

})

describe('#Toggle()', () => {

    describe('Button', () => {

        test('Toggle the [aria-pressed] and [aria-expanded] attribute on the <button>', () => {
            expect(fakeToggleButton._element.getAttribute('aria-pressed')).toBe('false')
            expect(fakeToggleButton._element.getAttribute('aria-expanded')).toBe('false')
            fireEvent(fakeToggleButton._element, new MouseEvent('click'))
            expect(fakeToggleButton._element.getAttribute('aria-pressed')).toBe('true')
            expect(fakeToggleButton._element.getAttribute('aria-expanded')).toBe('true')
            fireEvent(fakeToggleButton._element, new MouseEvent('click'))
            expect(fakeToggleButton._element.getAttribute('aria-pressed')).toBe('false')
            expect(fakeToggleButton._element.getAttribute('aria-expanded')).toBe('false')
        })

        test('Toggle the [hidden] attribute on the <div>', () => {
            const div = document.getElementById('fakeCollapse')
            expect(div.hidden).toBeTruthy()
            fireEvent(fakeToggleButton._element, new MouseEvent('click'))
            expect(div.hidden).toBeFalsy()
            fireEvent(fakeToggleButton._element, new MouseEvent('click'))
            expect(div.hidden).toBeTruthy()
        })

    })

    describe('Checkbox', () => {

        test('Toggle the [aria-expanded] attribute on the <checkbox>', () => {
            expect(fakeToggleCheckbox._element.getAttribute('aria-expanded')).toBe('false')
            fireEvent.change(fakeToggleCheckbox._element, { target: { checked: true } })
            expect(fakeToggleCheckbox._element.getAttribute('aria-expanded')).toBe('true')
            fireEvent.change(fakeToggleCheckbox._element, { target: { checked: false } })
            expect(fakeToggleCheckbox._element.getAttribute('aria-expanded')).toBe('false')
        })

        test('Toggle the [hidden] attribute on the <div>', () => {
            const div = document.getElementById('fakeCollapseCheckbox')
            expect(div.hidden).toBeTruthy()
            fireEvent.change(fakeToggleCheckbox._element, { target: { checked: true } })
            expect(div.hidden).toBeFalsy()
            fireEvent.change(fakeToggleCheckbox._element, { target: { checked: false } })
            expect(div.hidden).toBeTruthy()
        })

    })

    describe('Radio', () => {

        test('Toggle the [aria-expanded] attribute on the <radio>', () => {
            expect(fakeToggleRadioA._element.getAttribute('aria-expanded')).toBe('false')
            expect(fakeToggleRadioB._element.getAttribute('aria-expanded')).toBe('false')
            fireEvent.change(fakeToggleRadioA._element, { target: { checked: true } })
            expect(fakeToggleRadioA._element.getAttribute('aria-expanded')).toBe('true')
            expect(fakeToggleRadioB._element.getAttribute('aria-expanded')).toBe('false')
            fireEvent.change(fakeToggleRadioB._element, { target: { checked: true } })
            expect(fakeToggleRadioA._element.getAttribute('aria-expanded')).toBe('false')
            expect(fakeToggleRadioB._element.getAttribute('aria-expanded')).toBe('true')
        })

        test('Toggle the [hidden] attribute on the <div>', () => {
            const divA = document.getElementById('fakeCollapseRadioA')
            const divB = document.getElementById('fakeCollapseRadioB')
            expect(divA.hidden).toBeTruthy()
            expect(divB.hidden).toBeFalsy()
            fireEvent.change(fakeToggleRadioA._element, { target: { checked: true } })
            expect(divA.hidden).toBeFalsy()
            expect(divB.hidden).toBeTruthy()
            fireEvent.change(fakeToggleRadioB._element, { target: { checked: true } })
            expect(divA.hidden).toBeTruthy()
            expect(divB.hidden).toBeFalsy()
        })

    })

    describe('Select', () => {

        test('Toggle the [aria-expanded] attribute on the <select>', () => {
            expect(fakeToggleSelect._element.getAttribute('aria-expanded')).toBe('false')
            fireEvent.change(fakeToggleSelect._element, { target: { value: 'A' } })
            expect(fakeToggleSelect._element.getAttribute('aria-expanded')).toBe('true')
            fireEvent.change(fakeToggleSelect._element, { target: { value: 'null' } })
            expect(fakeToggleSelect._element.getAttribute('aria-expanded')).toBe('false')
        })

        test('Toggle the [hidden] attribute on the <div>', () => {
            const divA = document.getElementById('fakeCollapseSelectA')
            const divB = document.getElementById('fakeCollapseSelectB')
            const divC = document.getElementById('fakeCollapseSelectB')
            expect(divA.hidden).toBeTruthy()
            expect(divB.hidden).toBeTruthy()
            expect(divC.hidden).toBeTruthy()
            fireEvent.change(fakeToggleSelect._element, { target: { value: 'A' } })
            expect(divA.hidden).toBeFalsy()
            expect(divB.hidden).toBeTruthy()
            expect(divC.hidden).toBeTruthy()
            fireEvent.change(fakeToggleSelect._element, { target: { value: 'B' } })
            expect(divA.hidden).toBeTruthy()
            expect(divB.hidden).toBeFalsy()
            expect(divC.hidden).toBeFalsy()
            fireEvent.change(fakeToggleSelect._element, { target: { value: 'null' } })
            expect(divA.hidden).toBeTruthy()
            expect(divB.hidden).toBeTruthy()
            expect(divC.hidden).toBeTruthy()
        })

    })

    test('Emmit the toggle:changed event', () => {
        const eventSpy = vi.spyOn(fakeToggleButton, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeToggleButton._element, new MouseEvent('click'))
        expect(eventSpy).toHaveBeenCalledWith('changed')
    })

})