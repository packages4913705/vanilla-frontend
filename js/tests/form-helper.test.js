/**
 * ------------------------------------------------------------------
 * TEST for the utilities/_form-helper.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * - TogglePassword(): Passing the correct parameters
 * - TogglePassword(): Toggle the [type] attribute on the <input>
 * - TogglePassword(): Toggle the [aria-pressed] attribute on the <button>
 * - ToggleAttributes(): Passing the correct parameters
 * - ToggleAttributes(): Reset the [value] attribute on the <input>
 * - ToggleAttributes(): Toggle the [disabled] attribute on the <input>
 * - ToggleAttributes(): Toggle the [required] attribute on the <input>
 * - ToggleAttributes(): Toggle do not impact <input> if in unchanged option
 */

import { describe, test, expect, beforeAll, afterEach } from "vitest"
import FormHelper from "../utilities/_form-helper"
import ErrorMessage from "../utilities/_error"

let fakeInput, fakeButton, fakeFields, fieldA, fieldB

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(FormHelper.togglePassword).toBeTypeOf('function')
    })

})

describe('TogglePassword()', () => {

    beforeAll(() => {

        document.body.innerHTML =
            '<input id="fakeInput" type="password" value="123456">' +
            '<button id="fakeButton" aria-pressed="false" aria-controls="fakeInput"></button>'

        fakeInput = document.getElementById('fakeInput')
        fakeButton = document.getElementById('fakeButton')

    })

    test('Passing the correct parameters', () => {
        const button = document.createElement('button')
        expect(() => FormHelper.togglePassword('make-error')).toThrowError(ErrorMessage.instanceOf('button', 'HTMLElement'))
        expect(() => FormHelper.togglePassword(button)).toThrowError(ErrorMessage.withAttribute('button', 'aria-controls'))
        button.setAttribute('aria-controls', 'make-error')
        expect(() => FormHelper.togglePassword(button)).toThrowError(ErrorMessage.existById('input', 'make-error'))
    })

    test('Toggle the [type] attribute on the <input>', () => {
        expect(fakeInput.type).toBe('password')
        FormHelper.togglePassword(fakeButton)
        expect(fakeInput.type).toBe('text')
        FormHelper.togglePassword(fakeButton)
        expect(fakeInput.type).toBe('password')
    })

    test('Toggle the [aria-pressed] attribute on the <button>', () => {
        expect(fakeButton.getAttribute('aria-pressed')).toBe('false')
        FormHelper.togglePassword(fakeButton)
        expect(fakeButton.getAttribute('aria-pressed')).toBe('true')
    })

})

describe('ToggleAttributes', () => {

    beforeAll(() => {
        document.body.innerHTML =
            '<input id="fakeFieldA" name="fakeFieldA" type="text" value="aaa">' +
            '<input id="fakeFieldB" name="fakeFieldB" type="text" value="bbb">'

        fakeFields = document.querySelectorAll('#fakeFieldA,#fakeFieldB')
        fieldA = document.getElementById('fakeFieldA')
        fieldB = document.getElementById('fakeFieldB')
    })

    afterEach(() => {
        fieldA.value = 'aaa'
        fieldA.disabled = false
        fieldA.required = false
        fieldB.value = 'bbb'
        fieldB.disabled = false
        fieldB.required = false
    })

    test('Passing the correct parameters', () => {
        expect(() => FormHelper.toggleAttributes('make-error')).toThrowError(ErrorMessage.instanceOf('fields', 'Object'))
        expect(() => FormHelper.toggleAttributes(fakeFields, 'make-error')).toThrowError(ErrorMessage.typeOf('isVisible', 'boolean'))
        expect(() => FormHelper.toggleAttributes(fakeFields, true, 'make-error')).toThrowError(ErrorMessage.instanceOf('customOptions', 'Object'))
        expect(() => FormHelper.toggleAttributes(fakeFields, true, { reset: 'make-error' })).toThrowError(ErrorMessage.typeOf('customOptions.reset', 'boolean|object'))
        expect(() => FormHelper.toggleAttributes(fakeFields, true, { disabled: 'make-error' })).toThrowError(ErrorMessage.typeOf('customOptions.disabled', 'boolean|object'))
        expect(() => FormHelper.toggleAttributes(fakeFields, true, { required: 'make-error' })).toThrowError(ErrorMessage.typeOf('customOptions.required', 'boolean|object'))
        expect(() => FormHelper.toggleAttributes(fakeFields, true, { unchanged: 'make-error' })).toThrowError(ErrorMessage.typeOf('customOptions.unchanged', 'object'))
    })

    test('Reset the [value] attribute on the <input>', () => {
        expect(fieldA.value).toBe('aaa')
        expect(fieldB.value).toBe('bbb')
        FormHelper.toggleAttributes(fakeFields, true, { reset: false })
        expect(fieldA.value).toBe('aaa')
        expect(fieldB.value).toBe('bbb')
        FormHelper.toggleAttributes(fakeFields, false, { reset: ['fakeFieldA'] })
        expect(fieldA.value).toBe('null')
        expect(fieldB.value).toBe('bbb')
    })

    test('Toggle the [disabled] attribute on the <input>', () => {
        expect(fieldA.disabled).toBeFalsy()
        expect(fieldB.disabled).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, true, { disabled: false })
        expect(fieldA.disabled).toBeFalsy()
        expect(fieldB.disabled).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, false, { disabled: ['fakeFieldA'] })
        expect(fieldA.disabled).toBeTruthy()
        expect(fieldB.disabled).toBeFalsy()
    })

    test('Toggle the [required] attribute on the <input>', () => {
        expect(fieldA.required).toBeFalsy()
        expect(fieldB.required).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, true, { required: false })
        expect(fieldA.required).toBeFalsy()
        expect(fieldB.required).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, true, { required: ['fakeFieldA'] })
        expect(fieldA.required).toBeTruthy()
        expect(fieldB.required).toBeFalsy()
    })

    test('Toggle do not impact <input> if in unchanged option', () => {
        expect(fieldA.value).toBe('aaa')
        expect(fieldA.disabled).toBeFalsy()
        expect(fieldA.required).toBeFalsy()
        expect(fieldB.value).toBe('bbb')
        expect(fieldB.disabled).toBeFalsy()
        expect(fieldB.required).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, true, { reset: true, disabled: true, required: true, unchanged: ['fakeFieldB'] })
        expect(fieldA.value).toBe('aaa')
        expect(fieldA.disabled).toBeFalsy()
        expect(fieldA.required).toBeTruthy()
        expect(fieldB.value).toBe('bbb')
        expect(fieldB.disabled).toBeFalsy()
        expect(fieldB.required).toBeFalsy()
        FormHelper.toggleAttributes(fakeFields, false, { reset: true, disabled: true, required: true, unchanged: ['fakeFieldB'] })
        expect(fieldA.value).toBe('null')
        expect(fieldA.disabled).toBeTruthy()
        expect(fieldA.required).toBeFalsy()
        expect(fieldB.value).toBe('bbb')
        expect(fieldB.disabled).toBeFalsy()
        expect(fieldB.required).toBeFalsy()
    })

})