/**
 * ------------------------------------------------------------------
 * TEST for the _dialog.js
 * ------------------------------------------------------------------
 * The test will take care of:
 *  - Has all the public method
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - Open(): Add the [open] attribute on the <dialog>
 * - Open(): Emmit the dialog:opening & dialog:opened events
 * - Close(): Remove the [open] attribute on the <dialog>
 * - Close(): Emmit the dialog:closing & dialog:closed events
 * - Submit(): By default will exectute submit event
 * - Submit(): By JS will Remove the [open] attribute on the <dialog>
 * - Submit(): Toggle the [aria-busy] attribute on the <button>
 * - Submit(): Emmit the dialog:submiting & dialog:submited events
 * - #toggleBodyAttribute(): As modal will toggle the [inert] attribute on the <body>
 * - #toggleBodyAttribute(): As dialog will NOT toggle the [inert] attribute on the <body>
 * - #toggleBodyAttribute(): The [inert] attribute on the <body> will be removed only if all modal are closed
 */

import { describe, test, expect, beforeAll, vi, beforeEach, afterEach } from "vitest"
import Dialog from "../_dialog"

let fakeDialog, fakeModal, fakeFormModal

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<button aria-controls="fakeDialog"></button>' +
        '<dialog id="fakeDialog">' +
        '<button data-dialog-close></button>' +
        '</dialog>' +
        '<dialog id="fakeModal" aria-modal="true"></dialog>' +
        '<dialog id="fakeFormModal" aria-modal="true">' +
        '<form><button type="submit"></button></form>' +
        '</dialog>'

    fakeDialog = new Dialog(document.getElementById('fakeDialog'))
    fakeModal = new Dialog(document.getElementById('fakeModal'))
    fakeFormModal = new Dialog(document.getElementById('fakeFormModal'))

})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeDialog.open).toBeTypeOf('function')
        expect(fakeDialog.close).toBeTypeOf('function')
        expect(fakeDialog.submit).toBeTypeOf('function')
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeDialog).toHaveProperty('_isModal')
        expect(fakeDialog._isModal).toBeTypeOf('boolean')
        expect(fakeDialog._isModal).toBe(false)
        expect(fakeModal._isModal).toBe(true)
        expect(fakeDialog).toHaveProperty('_buttons')
        expect(fakeDialog._buttons).toBeTypeOf('object')
        expect(fakeDialog._buttons.open).toBeTypeOf('object')
        expect(fakeDialog._buttons.close).toBeTypeOf('object')
        expect(fakeDialog._buttons.submit).toBeTypeOf('object')
        expect(fakeDialog._buttons.open).toStrictEqual(document.querySelectorAll('[aria-controls=fakeDialog]'))
        expect(fakeDialog._buttons.close).toStrictEqual(fakeDialog._element.querySelectorAll('[data-dialog-close]'))
        expect(fakeDialog._buttons.submit).toStrictEqual(fakeDialog._element.querySelectorAll('[type=submit]'))
    })

})

describe('Open()', () => {

    afterEach(() => {
        fakeDialog._element.removeAttribute('open')
    })

    test('Add the [open] attribute on the <dialog>', () => {
        expect(fakeDialog._element.open).toBeFalsy()
        fakeDialog.open()
        expect(fakeDialog._element.open).toBeTruthy()
    })

    test('Emmit the dialog:opening & dialog:opened events', () => {
        const eventSpy = vi.spyOn(fakeDialog, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fakeDialog.open()
        expect(eventSpy).toHaveBeenCalledWith('opening')
        expect(eventSpy).toHaveBeenCalledWith('opened')
    })

})

describe('Close()', () => {

    beforeEach(() => {
        fakeDialog._element.setAttribute('open', true)
    })

    test('Remove the [open] attribute on the <dialog>', () => {
        expect(fakeDialog._element.open).toBeTruthy()
        fakeDialog.close()
        expect(fakeDialog._element.open).toBeFalsy()
    })

    test('Emmit the dialog:closing & dialog:closed events', () => {
        const eventSpy = vi.spyOn(fakeDialog, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fakeDialog.close()
        expect(eventSpy).toHaveBeenCalledWith('closing')
        expect(eventSpy).toHaveBeenCalledWith('closed')
    })

})

describe('Submit()', () => {

    beforeEach(() => {
        fakeFormModal._element.setAttribute('open', true)
    })

    test('By default will exectute submit event', () => {
        fakeFormModal._form.setAttribute('method', 'POST')
        const submitSpy = vi.spyOn(fakeFormModal._form, 'submit')
        expect(submitSpy).not.toHaveBeenCalled()
        fakeFormModal.submit()
        expect(submitSpy).toHaveBeenCalled()
    })

    test('By JS will Remove the [open] attribute on the <dialog>', () => {
        fakeFormModal._form.setAttribute('method', 'dialog')
        expect(fakeFormModal._element.open).toBeTruthy()
        fakeFormModal.submit()
        expect(fakeFormModal._element.open).toBeFalsy()
    })

    test('Toggle the [aria-busy] attribute on the <button>', (t) => {
        fakeFormModal._form.setAttribute('method', 'dialog')
        const button = fakeFormModal._buttons.submit[0]
        const testBefore = (e) => expect(button.hasAttribute('aria-busy')).toBeTruthy()
        fakeFormModal._element.addEventListener('dialog:submiting', testBefore)
        const testAfter = (e) => expect(button.hasAttribute('aria-busy')).toBeFalsy()
        fakeFormModal._element.addEventListener('dialog:submited', testAfter)
        fakeFormModal.submit()
        t.onTestFinished(() => fakeFormModal._element.removeEventListener('dialog:submiting', testBefore))
        t.onTestFinished(() => fakeFormModal._element.removeEventListener('dialog:submited', testAfter))
    })

    test('Emmit the dialog:submiting & dialog:submited events', () => {
        fakeFormModal._form.setAttribute('method', 'dialog')
        const eventSpy = vi.spyOn(fakeFormModal, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fakeFormModal.submit()
        expect(eventSpy).toHaveBeenCalledWith('submiting', { form: fakeFormModal._form })
        expect(eventSpy).toHaveBeenCalledWith('submited', { form: fakeFormModal._form })
    })

})

describe('#ToggleBodyAttribute()', () => {

    test('As modal will toggle the [inert] attribute on the <body>', () => {
        expect(document.body.hasAttribute('inert')).toBeFalsy()
        fakeModal.open()
        expect(document.body.hasAttribute('inert')).toBeTruthy()
        fakeModal.close()
        expect(document.body.hasAttribute('inert')).toBeFalsy()
    })

    test('As dialog will NOT toggle the [inert] attribute on the <body>', () => {
        expect(document.body.hasAttribute('inert')).toBeFalsy()
        fakeDialog.open()
        expect(document.body.hasAttribute('inert')).toBeFalsy()
    })

    test('The [inert] attribute on the <body> will be removed only if all modal are closed', () => {
        fakeModal.open()
        fakeFormModal.open()
        expect(document.body.hasAttribute('inert')).toBeTruthy()
        fakeModal.close()
        expect(document.body.hasAttribute('inert')).toBeTruthy()
        fakeFormModal.close()
        expect(document.body.hasAttribute('inert')).toBeFalsy()
    })

})