/**
 * ------------------------------------------------------------------
 * TEST for the utilities/_base-component.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * - Constructor: Passing the correct parameters
 * - Constructor: Return the correct properties
 * - MergeObject(): Passing the correct parameter
 * - MergeObject(): Return the correct value
 * - MergeObject(): Multilevel object
 * - EmmitEvent(): Passing the correct parameter
 * - EmmitEvent(): Return the correct value
 * - EmmitEvent(): Event listener
 * - EmmitEvent(): Prevent default
 */

import { describe, test, expect, beforeAll } from "vitest"
import { fireEvent } from "@testing-library/dom"
import BaseComponent from "../utilities/_base-component"
import ErrorMessage from "../utilities/_error"

let fakeDiv, fakeComponent, fakeEvent

/**
 * Before all tests
 * 
 */

beforeAll(() => {
    fakeDiv = document.createElement('div')
    fakeComponent = new BaseComponent(fakeDiv, {})
    fakeEvent = fakeComponent.emmitEvent('testing', { test: true }, fakeDiv)
})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeComponent.emmitEvent).toBeTypeOf('function')
    })

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new BaseComponent()).toThrowError(ErrorMessage.instanceOf('el', 'HTMLElement'))
        expect(() => new BaseComponent('make-error')).toThrowError(ErrorMessage.instanceOf('el', 'HTMLElement'))
        expect(() => new BaseComponent(fakeDiv, 'make-error')).toThrowError(ErrorMessage.instanceOf('options', 'Object'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeComponent).toHaveProperty('_element')
        expect(fakeComponent).toHaveProperty('_options')
        expect(fakeComponent._element).toBe(fakeDiv)
        expect(fakeComponent._options).toStrictEqual({})
    })

})

describe('MergeObject()', () => {

    test('Passing the correct parameter', () => {
        expect(() => fakeComponent.mergeObject('make-error', {})).toThrowError(ErrorMessage.instanceOf('one', 'Object'))
        expect(() => fakeComponent.mergeObject({}, 'make-error')).toThrowError(ErrorMessage.instanceOf('two', 'Object'))
    })

    test('Return the correct value', () => {
        const a = { one: 1, two: 2, tree: 3 }
        const b = { tree: 'three', four: 4 }
        const result = fakeComponent.mergeObject(a, b)
        expect(result).toStrictEqual({ one: 1, two: 2, tree: 'three' })
    })

    test('Multilevel object', () => {
        const a = { level: 1, sublevel: { one: 1, two: 2 } }
        const b = { level: 'one', sublevel: { one: 'one' } }
        const result = fakeComponent.mergeObject(a, b)
        expect(result).toStrictEqual({ level: 'one', sublevel: { one: 'one', two: 2 } })
    })

})

describe('EmmitEvent()', () => {

    test('Passing the correct parameter', () => {
        expect(() => fakeComponent.emmitEvent()).toThrowError(ErrorMessage.typeOf('name', 'string'))
        expect(() => fakeComponent.emmitEvent('testing', 'make-error')).toThrowError(ErrorMessage.instanceOf('data', 'Object'))
        expect(() => fakeComponent.emmitEvent('testing', {}, 'make-error')).toThrowError(ErrorMessage.instanceOf('selector', 'HTMLElement'))
    })

    test('Return the correct value', () => {
        expect(fakeEvent).toBeInstanceOf(CustomEvent)
        expect(fakeEvent.type).toBe('basecomponent:testing')
        expect(fakeEvent.detail).toStrictEqual({ test: true })
    })

    test('Event listener', () => {
        let steps = 0
        fakeDiv.addEventListener('basecomponent:testing', () => steps++)
        const myEvent = fireEvent(fakeDiv, fakeEvent)
        expect(myEvent).toBe(true)
        expect(steps).toBe(1)
    })

    test('Prevent default', () => {
        fakeDiv.addEventListener('basecomponent:testing', (e) => e.preventDefault())
        const myEvent = fireEvent(fakeDiv, fakeEvent)
        expect(myEvent).toBe(false)
    })

})