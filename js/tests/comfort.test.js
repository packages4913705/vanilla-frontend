/**
 * ------------------------------------------------------------------
 * TEST for the _comfort.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * ! herit from Cookie - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init: Emit the setTheme() method when click on this._buttons.theme
 * - #Init: Emit the setStyle() method when click on this._buttons.style 
 * - SetTheme(): Passing the correct parameters 
 * - SetTheme(): Save the property in the cookie
 * - SetTheme(): Emmit the set() method
 * - SetTheme(): If already exist, DONT emmit the set() method 
 * - SetStyle(): Passing the correct parameters 
 * - SetStyle(): Save the property in the cookie
 * - SetStyle(): Emmit the set() and #toggleStyle() method
 * - SetStyle(): If already exist, DONT emmit the set() and #toggleStyle() method 
 * - Reset(): Emmit de delete() and remove the cookie
 * - Reset(): Remove all the attributes
 * - #toggleTheme(): Add the [data-theme] attribute on the <html>
 * - #toggleTheme(): Add the [aria-pressed] attribute on the <button>
 * - #toggleStyle(): Add the [style] attribute on the <body>
 * - #toggleStyle(): Add the [aria-pressed] attribute on the <button>
 */

import { describe, test, expect, beforeAll, afterEach, afterAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Comfort from "../_comfort"
import ErrorMessage from "../utilities/_error"

let fakeComfort

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<button id="fakeDarkButton" data-theme="theme" value="dark"></button>' +
        '<button id="fakeLightButton" data-theme="theme" value="light"></button>' +
        '<button id="fakeFontButton" data-style="font-size" value="1rem"></button>'

    fakeComfort = new Comfort()

})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeComfort.setTheme).toBeTypeOf('function')
        expect(fakeComfort.setStyle).toBeTypeOf('function')
        expect(fakeComfort.reset).toBeTypeOf('function')
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeComfort._buttons).toBeTypeOf('object')
        expect(fakeComfort._buttons.theme).toStrictEqual(document.querySelectorAll('[data-theme]'))
        expect(fakeComfort._buttons.style).toStrictEqual(document.querySelectorAll('[data-style]'))
    })

})

describe('#Init()', () => {

    test('Emit the setTheme() method when click on this._buttons.theme', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'setTheme')
        fireEvent(document.getElementById('fakeDarkButton'), new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Emit the setStyle() method when click on this._buttons.style', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'setStyle')
        fireEvent(document.getElementById('fakeFontButton'), new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    afterAll(() => {
        fakeComfort.reset()
    })

})

describe('SetTheme()', () => {

    test('Passing the correct parameters', () => {
        expect(() => fakeComfort.setTheme()).toThrowError(ErrorMessage.typeOf('value', 'string'))
    })

    test('Save the property in the cookie', () => {
        expect(fakeComfort.has('theme')).toBeFalsy()
        fakeComfort.setTheme('my-theme')
        expect(fakeComfort.has('theme')).toBeTruthy()
        expect(fakeComfort.get('theme')).toBe('my-theme')
    })

    test('Emmit the set() method', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'set')
        fakeComfort.setTheme('dark')
        expect(methodSpy).toHaveBeenCalled()
    })

    test('If already exist, DONT emmit the set() method', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'set')
        fakeComfort.setTheme('dark')
        expect(methodSpy).not.toHaveBeenCalled()
    })

    afterAll(() => {
        fakeComfort.reset()
    })

})

describe('SetStyle()', () => {

    test('Passing the correct parameters', () => {
        expect(() => fakeComfort.setStyle()).toThrowError(ErrorMessage.typeOf('name', 'string'))
        expect(() => fakeComfort.setStyle('font-size')).toThrowError(ErrorMessage.typeOf('value', 'string'))
    })

    test('Save the property in the cookie', () => {
        expect(fakeComfort.has('style')).toBeFalsy()
        fakeComfort.setStyle('color', 'red')
        expect(fakeComfort.has('style')).toBeTruthy()
        expect(fakeComfort.get('style')).toStrictEqual({ 'color': 'red' })
    })

    test('Emmit the set() method', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'set')
        fakeComfort.setStyle('font-size', '1rem')
        expect(methodSpy).toHaveBeenCalled()
    })

    test('If already exist, DONT emmit the set() method', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'set')
        fakeComfort.setStyle('font-size', '1rem')
        expect(methodSpy).not.toHaveBeenCalled()
    })

    afterAll(() => {
        fakeComfort.reset()
    })

})

describe('Reset()', () => {

    test('Emmit de delete() and remove the cookie', () => {
        const methodSpy = vi.spyOn(fakeComfort, 'delete')
        fakeComfort.setTheme('dark')
        expect(fakeComfort.has('theme')).toBeTruthy()
        fakeComfort.reset()
        expect(fakeComfort.has('theme')).toBeFalsy()
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Remove all the attributes', () => {
        const html = document.documentElement
        const body = document.body
        const buttonTheme = document.getElementById('fakeDarkButton')
        const buttonStyle = document.getElementById('fakeFontButton')
        fakeComfort.setTheme('dark')
        fakeComfort.setStyle('font-size', '2rem')
        expect(html.hasAttribute('data-theme')).toBeTruthy()
        expect(body.hasAttribute('style')).toBeTruthy()
        expect(buttonTheme.hasAttribute('aria-pressed')).toBeTruthy()
        expect(buttonStyle.hasAttribute('aria-pressed')).toBeTruthy()
        fakeComfort.reset()
        expect(html.hasAttribute('data-theme')).toBeFalsy()
        expect(body.hasAttribute('style')).toBeFalsy()
        expect(buttonTheme.hasAttribute('aria-pressed')).toBeFalsy()
        expect(buttonStyle.hasAttribute('aria-pressed')).toBeFalsy()
    })

})


describe('#ToggleTheme()', () => {

    afterEach(() => {
        fakeComfort.reset()
    })

    test('Add the [data-theme] attribute on the <html>', () => {
        const html = document.documentElement
        expect(html.hasAttribute('data-theme')).toBeFalsy()
        fakeComfort.setTheme('dark')
        expect(html.hasAttribute('data-theme')).toBeTruthy()
        expect(html.getAttribute('data-theme')).toBe('dark')
    })

    test('Add the [aria-pressed] attribute on the <button>', () => {
        const button = document.getElementById('fakeDarkButton')
        expect(button.hasAttribute('aria-pressed')).toBeFalsy()
        fakeComfort.setTheme('dark')
        expect(button.hasAttribute('aria-pressed')).toBeTruthy()
        expect(button.getAttribute('aria-pressed')).toBe('true')
    })

    test('Add the [style] attribute on the <body>', () => {
        const body = document.body
        expect(body.hasAttribute('style')).toBeFalsy()
        fakeComfort.setStyle('font-size', '1rem')
        expect(body.hasAttribute('style')).toBeTruthy()
        expect(body.getAttribute('style')).toBe('font-size:1rem')
    })

    test('Add the [aria-pressed] attribute on the <button>', () => {
        const button = document.getElementById('fakeFontButton')
        expect(button.hasAttribute('aria-pressed')).toBeFalsy()
        fakeComfort.setStyle('font-size', '1rem')
        expect(button.hasAttribute('aria-pressed')).toBeTruthy()
        expect(button.getAttribute('aria-pressed')).toBe('true')
    })

})