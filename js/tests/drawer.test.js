/**
 * ------------------------------------------------------------------
 * TEST for the _drawer.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init: On click on the button toggle the drawer
 * - #Init: On click on the backdrop, close the drawer
 */

import { describe, test, expect, beforeAll } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Drawer from "../_drawer"
import ErrorMessage from "../utilities/_error"

let fakeDrawer

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<div id="backdrop"></div>' +
        '<button aria-expanded="false" aria-pressed="false" aria-controls="drawer"></button>' +
        '<aside id="drawer" hidden><button id="focus"></button></aside>'

    Object.defineProperty(window, 'innerWidth', { configurable: true, value: 1200 })

    fakeDrawer = new Drawer(document.getElementById('drawer'))

})

describe('Structure of the class', () => {

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Drawer(document.getElementById('drawer'), { breakpoint: 'make-error' })).toThrowError(ErrorMessage.typeOf('options.breakpoint', 'number'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeDrawer._buttons).toStrictEqual(document.querySelectorAll('button[aria-controls="drawer"]'))
        expect(fakeDrawer._backdrop).toStrictEqual(document.getElementById('backdrop'))
        expect(fakeDrawer._isOpen).toBeTruthy()
        expect(fakeDrawer._focus).toStrictEqual(document.getElementById('focus'))
    })

})

describe('#Init(', () => {

    test('On click on the button toggle the drawer', () => {

        const button = fakeDrawer._buttons[0]

        expect(fakeDrawer._isOpen).toBeTruthy()
        expect(fakeDrawer._element.hasAttribute('hidden')).toBeFalsy()
        expect(button.getAttribute('aria-pressed')).toBe('true')
        expect(button.getAttribute('aria-expanded')).toBe('true')

        fireEvent(button, new MouseEvent('click'))

        expect(fakeDrawer._isOpen).toBeFalsy()
        expect(fakeDrawer._element.hasAttribute('hidden')).toBeTruthy()
        expect(button.getAttribute('aria-pressed')).toBe('false')
        expect(button.getAttribute('aria-expanded')).toBe('false')

        fireEvent(button, new MouseEvent('click'))

        expect(fakeDrawer._isOpen).toBeTruthy()
        expect(fakeDrawer._element.hasAttribute('hidden')).toBeFalsy()
        expect(button.getAttribute('aria-pressed')).toBe('true')
        expect(button.getAttribute('aria-expanded')).toBe('true')

    })

    test('On click on the backdrop, close the drawer', () => {

        Object.defineProperty(window, 'innerWidth', { configurable: true, value: 800 })

        const button = fakeDrawer._buttons[0]

        expect(fakeDrawer._isOpen).toBeTruthy()
        expect(fakeDrawer._element.hasAttribute('hidden')).toBeFalsy()
        expect(button.getAttribute('aria-pressed')).toBe('true')
        expect(button.getAttribute('aria-expanded')).toBe('true')

        fireEvent(document.getElementById('backdrop'), new MouseEvent('click'))

        expect(fakeDrawer._isOpen).toBeFalsy()
        expect(fakeDrawer._element.hasAttribute('hidden')).toBeTruthy()
        expect(button.getAttribute('aria-pressed')).toBe('false')
        expect(button.getAttribute('aria-expanded')).toBe('false')

    })

})

