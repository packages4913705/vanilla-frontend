/**
 * ------------------------------------------------------------------
 * TEST for the _tree.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - #Init() : Do nothing if click outside the handle
 * - #Toggle(): Passing the correct parameter
 * - #Toggle(): List: Update the [aria-expanded] attribute
 * - #Toggle(): List: Update the [hidden] attribute on children
 * - #Toggle(): Grid: Update the [aria-expanded] attribute
 * - #Toggle(): Grid: Update the [hidden] attribute on children
 * - #Toggle(): Grid: Collapse the sub-children 
 * - #Toggle(): Emmit the tree:changed event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Tree from "../_tree"


let fakeListTree, fakeGridTree, fakeLi, fakeRow, customClick

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<ul id="fakeListTree">' +
        '<li></li>' +
        '<li>' +
        '<button role="link" aria-controls="child" aria-expanded="false">Toggle</button>' +
        '<ul id="child" hidden>' +
        '<li></li>' +
        '</ul>' +
        '</li>' +
        '<li></li>' +
        '</ul>' +
        '<table id="fakeGridTree">' +
        '<tr aria-level="1"><td></td></tr>' +
        '<tr aria-level="1" aria-expanded="false" aria-controls="lvl2"><td data-handle="tree"></td></tr>' +
        '<tr id="lvl2" aria-level="2" aria-expanded="false" aria-controls="lvl3" hidden><td data-handle="tree"></td></tr>' +
        '<tr id="lvl3" aria-level="3" hidden><td></td></tr>' +
        '<tr aria-level="1"><td></td></tr>' +
        '</table>'

    fakeListTree = new Tree(document.getElementById('fakeListTree'))
    fakeGridTree = new Tree(document.getElementById('fakeGridTree'))
    fakeLi = fakeListTree._element.querySelector('li [aria-expanded]')
    fakeRow = fakeGridTree._element.querySelector('tr[aria-level="1"][aria-expanded]')

    customClick = new MouseEvent('click')
    Object.defineProperty(customClick, 'target', { value: fakeRow.querySelector('[data-handle=tree]') }) // Define the click event with custom target

})

describe('Structure of the class', () => {

    test('Constructor: Return the correct properties', () => {
        expect(fakeListTree).toHaveProperty('_type')
        expect(fakeListTree).toHaveProperty('_withHandle')
        expect(fakeListTree).toHaveProperty('_items')
        expect(fakeListTree._type).toBe('list')
        expect(fakeGridTree._type).toBe('grid')
        expect(fakeListTree._withHandle).toBeFalsy()
        expect(fakeGridTree._withHandle).toBeTruthy()
        expect(fakeListTree._items).toBeTypeOf('object')
        expect(fakeListTree._items).toStrictEqual(document.querySelectorAll('#fakeListTree [aria-expanded]'))
    })

})

describe('#Init()', () => {

    test('Do nothing if click outside the handle', () => {
        expect(fakeRow.getAttribute('aria-expanded')).toBe('false')
        fireEvent(fakeRow, new MouseEvent('click'))
        expect(fakeRow.getAttribute('aria-expanded')).toBe('false')
    })

})

describe('#Toggle()', () => {

    describe('List', () => {

        test('Update the [aria-expanded] attribute', () => {
            expect(fakeLi.getAttribute('aria-expanded')).toBe('false')
            fireEvent(fakeLi, new MouseEvent('click'))
            expect(fakeLi.getAttribute('aria-expanded')).toBe('true')
            fireEvent(fakeLi, new MouseEvent('click'))
            expect(fakeLi.getAttribute('aria-expanded')).toBe('false')
        })

        test('Update the [hidden] attribute on children', () => {
            const child = document.getElementById('child')
            expect(child.hidden).toBeTruthy()
            fireEvent(fakeLi, new MouseEvent('click'))
            expect(child.hidden).toBeFalsy()
            fireEvent(fakeLi, new MouseEvent('click'))
            expect(child.hidden).toBeTruthy()
        })

    })

    describe('Grid', () => {

        test('Update the [aria-expanded] attribute', () => {
            expect(fakeRow.getAttribute('aria-expanded')).toBe('false')
            fireEvent(fakeRow, customClick)
            expect(fakeRow.getAttribute('aria-expanded')).toBe('true')
            fireEvent(fakeRow, customClick)
            expect(fakeRow.getAttribute('aria-expanded')).toBe('false')
        })

        test('Update the [hidden] attribute on children', () => {
            const child = document.getElementById('lvl2')
            expect(child.hidden).toBeTruthy()
            fireEvent(fakeRow, customClick)
            expect(child.hidden).toBeFalsy()
            fireEvent(fakeRow, customClick)
            expect(child.hidden).toBeTruthy()
        })

        test('Collapse the sub-children', () => {
            const lvl2 = document.getElementById('lvl2')
            const lvl3 = document.getElementById('lvl3')

            const customClickLvl2 = new MouseEvent('click')
            Object.defineProperty(customClickLvl2, 'target', { value: lvl2.querySelector('[data-handle=tree]') }) // Define the click event with custom target

            const customClickLvl3 = new MouseEvent('click')
            Object.defineProperty(customClickLvl3, 'target', { value: lvl3.querySelector('[data-handle=tree]') }) // Define the click event with custom target

            fireEvent(fakeRow, customClick)
            fireEvent(lvl2, customClickLvl2)
            fireEvent(lvl3, customClickLvl3)

            expect(fakeRow.getAttribute('aria-expanded')).toBe('true')
            expect(lvl2.hidden).toBeFalsy()
            expect(lvl2.getAttribute('aria-expanded')).toBe('true')
            expect(lvl3.hidden).toBeFalsy()

            fireEvent(fakeRow, customClick)

            expect(fakeRow.getAttribute('aria-expanded')).toBe('false')
            expect(lvl2.hidden).toBeTruthy()
            expect(lvl2.getAttribute('aria-expanded')).toBe('false')
            expect(lvl3.hidden).toBeTruthy()

        })

    })

    test('Emmit the tree:changed event', () => {
        const eventSpy = vi.spyOn(fakeListTree, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeLi, new MouseEvent('click'))
        expect(eventSpy).toHaveBeenCalledWith('changed', { isOpen: true })
    })

})