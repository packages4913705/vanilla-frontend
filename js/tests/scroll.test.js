/**
 * ------------------------------------------------------------------
 * TEST for the _scroll.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * - Constructor: Passing the correct parameters
 * - Constructor: Return the correct properties
 * - #Init: Emit the scrollTop() method when click on this._buttons.top
 * - #Init: Emit the scrollBottom() method when click on this._buttons.bottom
 * - GoTo(): Passing the correct parameters 
 * - GoTo(): Emit a scroll event 
 * - ScrollTop(): Emit a goTo() method and scrollTop is 0
 * - ScrollBottom(): Emit a goTo() method and scrollTop is this._height
 * - #Scroll(): Toggle the [hidden] attribute on the [data-scroll-top] button 
 * - #Scroll(): Toggle the [hidden] attribute on the [data-scroll-bottom] button
 * - #Spy(): Toggle the [aria-current] attribute on the <a> 
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Scroll from "../_scroll"
import ErrorMessage from "../utilities/_error"

let fakeDiv, fakeDivScroll, fakePageScroll, fakeDivOne, fakeDivTwo

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<div id="scrollspy"><a href="#one"></a><a href="#two"></a><a><a/></div>' +
        '<div id="fakeDiv" style="height:200px;overflow-y:scroll">' +
        '<button data-scroll-bottom="fakeDiv"></button>' +
        '<button data-scroll-top="fakeDiv" hidden></button>' +
        '<div id="one"></div>' +
        '<div id="two"></div>' +
        '</div>'

    fakeDiv = document.getElementById('fakeDiv')
    fakeDivOne = document.getElementById('one')
    fakeDivTwo = document.getElementById('two')

    // Define the sizes for HTML div
    Object.defineProperty(fakeDiv, 'scrollHeight', { configurable: true, value: 800 })
    Object.defineProperty(fakeDiv, 'clientHeight', { configurable: true, value: 200 })
    Object.defineProperty(fakeDivOne, 'offsetTop', { configurable: true, value: 200 })
    Object.defineProperty(fakeDivTwo, 'offsetTop', { configurable: true, value: 400 })
    Object.defineProperty(document.documentElement, 'scrollHeight', { configurable: true, value: 1200 })
    Object.defineProperty(document.documentElement, 'clientHeight', { configurable: true, value: 600 })

    // Then init the class
    fakeDivScroll = new Scroll(fakeDiv, { navigation: 'scrollspy', gaps: { spy: 0 } })
    fakePageScroll = new Scroll(document.documentElement)

})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeDivScroll.goTo).toBeTypeOf('function')
        expect(fakeDivScroll.scrollTop).toBeTypeOf('function')
        expect(fakeDivScroll.scrollBottom).toBeTypeOf('function')
    })

    test('Constructor: Passing the correct parameters', () => {
        expect(() => new Scroll(fakeDiv, { gaps: 'make-error' })).toThrowError(ErrorMessage.instanceOf('options.gaps', 'Object'))
        expect(() => new Scroll(fakeDiv, { gaps: { top: 'make-error' } })).toThrowError(ErrorMessage.typeOf('options.gaps.top', 'number'))
        expect(() => new Scroll(fakeDiv, { gaps: { bottom: 'make-error' } })).toThrowError(ErrorMessage.typeOf('options.gaps.bottom', 'number'))
        expect(() => new Scroll(fakeDiv, { gaps: { spy: 'make-error' } })).toThrowError(ErrorMessage.typeOf('options.gaps.spy', 'number'))
        expect(() => new Scroll(fakeDiv, { behavior: 'make-error' })).toThrowError(ErrorMessage.enumOf('options.behavior', 'auto|smooth|instant'))
        expect(() => new Scroll(fakeDiv, { navigation: 'make-error' })).toThrowError(ErrorMessage.existById('element', 'make-error'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeDivScroll).toHaveProperty('_height')
        expect(fakeDivScroll._height).toBe(600)
        expect(fakeDivScroll._buttons).toBeTypeOf('object')
        expect(fakeDivScroll._buttons.top).toStrictEqual(document.querySelector('[data-scroll-top="fakeDiv"]'))
        expect(fakeDivScroll._buttons.bottom).toStrictEqual(document.querySelector('[data-scroll-bottom="fakeDiv"]'))
        expect(fakePageScroll._buttons.top).toBeNull()
        expect(fakePageScroll._buttons.bottom).toBeNull()
        expect(fakeDivScroll._spy).toBeTypeOf('object')
        expect(fakeDivScroll._spy.enable).toBeTypeOf('boolean')
        expect(fakeDivScroll._spy.links).toBeTypeOf('object')
        expect(fakeDivScroll._spy.targets).toBeTypeOf('object')
        expect(fakeDivScroll._spy.current).toBeTypeOf('number')
        expect(fakeDivScroll._spy.enable).toBeTruthy()
        expect(fakePageScroll._spy.enable).toBeFalsy()
        expect(fakeDivScroll._spy.links).toStrictEqual([...document.querySelectorAll('#scrollspy a[href^="#"]')])
        expect(fakeDivScroll._spy.targets).toStrictEqual([...document.querySelectorAll('#one,#two')])
        expect(fakeDivScroll._spy.current).toBe(0)
    })

})

describe('#Init()', () => {

    test('Emit the scrollTop() method when click on this._buttons.top', () => {
        const methodSpy = vi.spyOn(fakeDivScroll, 'scrollTop')
        fireEvent(document.querySelector('[data-scroll-top="fakeDiv"]'), new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Emit the scrollBottom() method when click on this._buttons.bottom', () => {
        const methodSpy = vi.spyOn(fakeDivScroll, 'scrollBottom')
        fireEvent(document.querySelector('[data-scroll-bottom="fakeDiv"]'), new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Emit the goTo() method when click on this._spy.links', () => {
        const methodSpy = vi.spyOn(fakeDivScroll, 'goTo')
        fireEvent(document.querySelector('a[href="#one"]'), new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

})

describe('GoTo()', () => {

    test('Passing the correct parameters ', () => {
        expect(() => fakeDivScroll.goTo()).toThrowError(ErrorMessage.typeOf('distance', 'number'))
        expect(() => fakeDivScroll.goTo('make-error')).toThrowError(ErrorMessage.typeOf('distance', 'number'))
    })

    test('Emit a scroll event', () => {
        const scrollSpy = vi.spyOn(fakeDiv, 'scroll')
        fakeDivScroll.goTo(100)
        expect(scrollSpy).toHaveBeenCalled()
    })

})

describe('ScrollTop()', () => {

    test('Emit a goTo() method with parameter as 0', () => {
        const methodSpy = vi.spyOn(fakeDivScroll, 'goTo')
        fakeDivScroll.scrollTop()
        expect(methodSpy).toHaveBeenCalledWith(0)
    })

})

describe('ScrollBottom()', () => {

    test('Emit a goTo() method with parameter as _height', () => {
        const methodSpy = vi.spyOn(fakeDivScroll, 'goTo')
        fakeDivScroll.scrollBottom()
        expect(methodSpy).toHaveBeenCalledWith(fakeDivScroll._height)
    })

})

describe('#Scroll()', () => {

    test('Toggle the [hidden] attribute on the [data-scroll-top] button', () => {
        const topButton = document.querySelector('[data-scroll-top="fakeDiv"]')
        expect(topButton.hidden).toBeTruthy()
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 300 })
        fireEvent.scroll(fakeDiv)
        expect(topButton.hidden).toBeFalsy()
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 0 })
        fireEvent.scroll(fakeDiv)
        expect(topButton.hidden).toBeTruthy()
    })

    test('Toggle the [hidden] attribute on the [data-scroll-bottom] button', () => {
        const topButton = document.querySelector('[data-scroll-bottom="fakeDiv"]')
        expect(topButton.hidden).toBeFalsy()
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 600 })
        fireEvent.scroll(fakeDiv)
        expect(topButton.hidden).toBeTruthy()
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 0 })
        fireEvent.scroll(fakeDiv)
        expect(topButton.hidden).toBeFalsy()
    })

})

describe('#Spy()', () => {

    test('Toggle the [aria-current] attribute on the <a> ', () => {
        const one = document.querySelector('a[href="#one"]')
        const two = document.querySelector('a[href="#two"]')
        expect(fakeDivScroll._spy.current).toBe(2)
        expect(one.hasAttribute('aria-current')).toBeFalsy() //200
        expect(two.hasAttribute('aria-current')).toBeFalsy() //400
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 200 })
        fireEvent.scroll(fakeDiv)
        expect(fakeDivScroll._spy.current).toBe(0)
        expect(one.hasAttribute('aria-current')).toBeTruthy()
        expect(two.hasAttribute('aria-current')).toBeFalsy()
        Object.defineProperty(fakeDiv, 'scrollTop', { configurable: true, value: 400 })
        fireEvent.scroll(fakeDiv)
        expect(fakeDivScroll._spy.current).toBe(1)
        expect(one.hasAttribute('aria-current')).toBeFalsy()
        expect(two.hasAttribute('aria-current')).toBeTruthy()
    })

})