/**
 * ------------------------------------------------------------------
 * TEST for the _sortable.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * ! NONE ! - Has all the public method
 * ! Tested in BaseComponent ! - Constructor: Passing the correct parameters 
 * - Constructor: Return the correct properties
 * - Mousedown & Mouseup: Without handle toggle the [draggable] attribute
 * - Mousedown & Mouseup: With handle toggle the [draggable] attribute
 * - Drag(): Add the [aria-grabbed] attribute
 * - Drag(): Emmit the sortable:drag event
 * - Drop(): Remove the [aria-grabbed] attribute
 * - Drop(): Emmit the sortable:drop event
 */

import { describe, test, expect, beforeAll, vi } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Sortable from "../_sortable"

let fakeListSortable, fakeTableSortable, fakeListItem

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<ul id="fakeList">' +
        '<li id="first" draggable="false">One</li>' +
        '<li draggable="false">Two</li>' +
        '<li draggable="false">Three</li>' +
        '</ul>' +
        '<table id="fakeTable">' +
        '<tr draggable="false"><td data-handle="sortable">DRAG</td><td>One</td></tr>' +
        '<tr draggable="false"><td data-handle="sortable">DRAG</td><td>Two</td></tr>' +
        '<tr draggable="false"><td data-handle="sortable">DRAG</td><td>Three</td></tr>' +
        '</table>'

    fakeListSortable = new Sortable(document.getElementById('fakeList'))
    fakeTableSortable = new Sortable(document.getElementById('fakeTable'))
    fakeListItem = document.getElementById('first')

})

describe('Structure of the class', () => {

    test('Constructor: Return the correct properties', () => {
        expect(fakeListSortable).toHaveProperty('_withHandle')
        expect(fakeListSortable._withHandle).toBeTypeOf('boolean')
        expect(fakeListSortable._withHandle).toBe(false)
        expect(fakeTableSortable._withHandle).toBe(true)
        expect(fakeListSortable._items).toBeTypeOf('object')
        expect(fakeListSortable._items).toStrictEqual(document.querySelectorAll('#fakeList li'))
    })

})

describe('Mousedown & Mouseup', () => {

    test('Without handle toggle the [draggable] attribute', () => {
        expect(fakeListItem.draggable).toBeFalsy()
        fireEvent(fakeListItem, new MouseEvent('mousedown'))
        expect(fakeListItem.draggable).toBeTruthy()
        fireEvent(fakeListItem, new MouseEvent('mouseup'))
        expect(fakeListItem.draggable).toBeFalsy()
    })

    test('With handle & Mouseup toggle the [draggable] attribute', () => {
        const item = document.querySelector('#fakeTable tr')
        const handle = document.querySelector('#fakeTable tr:first-child [data-handle=sortable]')
        const customMousedown = new MouseEvent('mousedown')
        Object.defineProperty(customMousedown, 'target', { value: handle }) // Define the mousedown event with custom target
        expect(item.draggable).toBeFalsy()
        fireEvent(item, new MouseEvent('mousedown'))
        expect(item.draggable).toBeFalsy()
        fireEvent(item, customMousedown)
        expect(item.draggable).toBeTruthy()
        fireEvent(item, new MouseEvent('mouseup'))
        expect(item.draggable).toBeFalsy()
    })

})

describe('Drag()', () => {

    test('Add the [aria-grabbed] attribute', () => {
        expect(fakeListItem.hasAttribute('aria-grabbed')).toBeFalsy()
        fireEvent(fakeListItem, new MouseEvent('mousedown'))
        fireEvent(fakeListItem, new MouseEvent('dragstart'))
        expect(fakeListItem.hasAttribute('aria-grabbed')).toBeTruthy()
        fireEvent(fakeListItem, new MouseEvent('dragend'))
        fireEvent(fakeListItem, new MouseEvent('mouseup'))
        expect(fakeListItem.hasAttribute('aria-grabbed')).toBeFalsy()
    })

    test('Emmit the sortable:drag event', () => {
        const eventSpy = vi.spyOn(fakeListSortable, 'emmitEvent')
        expect(eventSpy).not.toHaveBeenCalled()
        fireEvent(fakeListItem, new MouseEvent('mousedown'))
        fireEvent(fakeListItem, new MouseEvent('dragstart'))
        expect(eventSpy).toHaveBeenCalledWith('drag', { current: fakeListItem, items: fakeListSortable._items })
    })

})

describe('Drop()', () => {

    test('Remove the [aria-grabbed] attribute', () => {
        expect(fakeListItem.hasAttribute('aria-grabbed')).toBeTruthy()
        fireEvent(fakeListItem, new MouseEvent('dragend'))
        expect(fakeListItem.hasAttribute('aria-grabbed')).toBeFalsy()
    })

    test('Emmit the sortable:drop event', () => {
        const eventSpy = vi.spyOn(fakeListSortable, 'emmitEvent')
        fireEvent(fakeListItem, new MouseEvent('mousedown')) // First drag item
        fireEvent(fakeListItem, new MouseEvent('dragstart')) // First drag item
        fireEvent(fakeListItem, new MouseEvent('dragend'))
        expect(eventSpy).toHaveBeenCalledWith('drop', { current: fakeListItem, items: fakeListSortable._items })
    })

})