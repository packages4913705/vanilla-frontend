/**
 * ------------------------------------------------------------------
 * TEST for the /_slider.js
 * ------------------------------------------------------------------
 * The test will take care of:
 * - Has all the public method
 * ! herit from BaseComponent - Constructor: Passing the correct parameters 
 * - Constructor: Passing the correct parameters
 * - Constructor: Return the correct properties
 * - #Init(): Loop will clone the first and last slide
 * - #Init(): Autoplay will emmit next() method every X ms
 * - #Init(): Click on <button> with [role=tab] emmit the goTo() method
 * - #Init(): Click on <button> with [data-slider-prev] emmit the prev() method
 * - #Init(): Click on <button> with [data-slider-next] emmit the next() method
 * - GoTo(): Passing the correct parameters
 * - GoTo(): Go to the first or last slide via toGo() method
 * - GoTo(): Change the [aria-selected], [aria-hidden] and [disabled] attributes
 * - GoTo(): Emmit the slider:changed events
 * - Prev(): Emmit the goTo() method with correct index
 * - Prev(): Don't emmit the goTo() method if already first slide
 * - Next(): Emmit the goTo() method with correct index
 * - Next(): Don't emmit the goTo() method if already last slide

 */

import { describe, test, expect, beforeAll, vi, afterAll } from "vitest"
import { fireEvent } from "@testing-library/dom"
import Slider from "../_slider"
import ErrorMessage from "../utilities/_error"

let fakeDiv, fakeSlider, fakeDivLoop, fakeSliderLoop

/**
 * Before all tests
 * 
 */

beforeAll(() => {

    document.body.innerHTML =
        '<div id="slider">' +
        '<div id="1" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=one"></div>' +
        '<div id="2" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=two"></div>' +
        '<div id="3" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=three"></div>' +
        '</div>' +
        '<div id="sliderLoop">' +
        '<div id="4" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=one"></div>' +
        '<div id="5" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=two"></div>' +
        '<div id="6" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=three"></div>' +
        '</div>' +
        '<div id="sliderAuto">' +
        '<div id="7" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=one"></div>' +
        '<div id="8" role="tabpanel" aria-hidden="false"><img src="https://fakeimg.pl/760x506/?text=two"></div>' +
        '</div>' +
        '<div aria-controls="slider" role="tablist">' +
        '<button role="tab" aria-controls="1" aria-selected="true">1</button>' +
        '<button role="tab" aria-controls="2" aria-selected="false">2</button>' +
        '<button role="tab" aria-controls="3" aria-selected="false">3</button>' +
        '</div>' +
        '<div>' +
        '<button aria-controls="slider" data-slider-prev>previous</button>' +
        '<button aria-controls="slider" data-slider-next>next</button>' +
        '</div>'

    fakeDiv = document.getElementById('slider')
    Object.defineProperty(fakeDiv, 'scrollWidth', { configurable: true, value: 300 })
    Object.defineProperty(fakeDiv, 'clientWidth', { configurable: true, value: 100 })

    fakeDivLoop = document.getElementById('sliderLoop')
    Object.defineProperty(fakeDivLoop, 'scrollWidth', { configurable: true, value: 300 })
    Object.defineProperty(fakeDivLoop, 'offsetWidth', { configurable: true, value: 100 })
    Object.defineProperty(fakeDivLoop, 'clientWidth', { configurable: true, value: 100 })

    fakeSlider = new Slider(fakeDiv)
    fakeSliderLoop = new Slider(fakeDivLoop, { loop: true })

    Object.defineProperty(fakeSlider._slides[0], 'offsetLeft', { configurable: true, value: 0 })
    Object.defineProperty(fakeSlider._slides[1], 'offsetLeft', { configurable: true, value: 100 })
    Object.defineProperty(fakeSlider._slides[2], 'offsetLeft', { configurable: true, value: 200 })

    let val = 0
    fakeSliderLoop._element.children.forEach(child => {
        Object.defineProperty(child, 'offsetLeft', { configurable: true, value: val })
        val = val + 100
    })

})

describe('Structure of the class', () => {

    test('Has all the public method', () => {
        expect(fakeSlider.goTo).toBeTypeOf('function')
        expect(fakeSlider.prev).toBeTypeOf('function')
        expect(fakeSlider.next).toBeTypeOf('function')
    })

    test('Constructor: Passing the correct parameters', () => {
        const htmlSlider = document.getElementById('slider')
        expect(() => new Slider(htmlSlider, { behavior: 'make-error' })).toThrowError(ErrorMessage.enumOf('options.behavior', 'auto|smooth|instant'))
        expect(() => new Slider(htmlSlider, { loop: 'make-error' })).toThrowError(ErrorMessage.typeOf('options.loop', 'boolean'))
        expect(() => new Slider(htmlSlider, { autoplay: 'make-error' })).toThrowError(ErrorMessage.typeOf('options.autoplay', 'boolean|number'))
    })

    test('Constructor: Return the correct properties', () => {
        expect(fakeSlider).toHaveProperty('_element')
        expect(fakeSlider).toHaveProperty('_options')
        expect(fakeSlider).toHaveProperty('_slides')
        expect(fakeSlider).toHaveProperty('_buttons')
        expect(fakeSlider).toHaveProperty('_current')
        expect(fakeSlider).toHaveProperty('_interval')
        expect(fakeSlider._slides).toBeTypeOf('object')
        expect(fakeSlider._slides).toStrictEqual(Array.from(document.querySelectorAll('#slider [role=tabpanel]')))
        expect(fakeSlider._buttons).toBeTypeOf('object')
        expect(fakeSlider._buttons.prev).toBeTypeOf('object')
        expect(fakeSlider._buttons.next).toBeTypeOf('object')
        expect(fakeSlider._buttons.tabs).toBeTypeOf('object')
        expect(fakeSlider._buttons.prev).toStrictEqual(document.querySelector('[data-slider-prev]'))
        expect(fakeSlider._buttons.next).toStrictEqual(document.querySelector('[data-slider-next]'))
        expect(fakeSlider._buttons.tabs).toStrictEqual(document.querySelectorAll('[role=tablist] [role=tab]'))
        expect(fakeSlider._current).toBe(0)
        expect(fakeSlider._interval).toBeNull()
    })

})

describe('#Init()', () => {

    test('Loop will clone the first and last slide', () => {
        expect(fakeSliderLoop._slides.length).toBe(3)
        expect(fakeSliderLoop._element.children.length).toBe(5)
        expect(fakeSliderLoop._element.firstElementChild.querySelector('img').getAttribute('src')).toContain('three')
        expect(fakeSliderLoop._element.lastElementChild.querySelector('img').getAttribute('src')).toContain('one')
    })

    test('Autoplay will emmit next() method every X ms', () => {
        vi.useFakeTimers()
        const fakeSliderAuto = new Slider(document.getElementById('sliderAuto'), { autoplay: 1500 })
        const methodSpy = vi.spyOn(fakeSliderAuto, 'next')
        vi.advanceTimersByTime(3000)
        expect(methodSpy).toHaveBeenCalledTimes(2)
        clearInterval(fakeSliderAuto._interval)
    })

    test('Click on <button> with [role=tab] emmit the goTo() method', () => {
        const fakeTab = fakeSlider._buttons.tabs[0]
        const methodSpy = vi.spyOn(fakeSlider, 'goTo')
        fireEvent(fakeTab, new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Click on <button> with [data-slider-prev] emmit the prev() method', () => {
        const fakePrev = fakeSlider._buttons.prev
        const methodSpy = vi.spyOn(fakeSlider, 'prev')
        fireEvent(fakePrev, new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

    test('Click on <button> with [data-slider-next] emmit the next() method', () => {
        const fakeNext = fakeSlider._buttons.next
        const methodSpy = vi.spyOn(fakeSlider, 'next')
        fireEvent(fakeNext, new MouseEvent('click'))
        expect(methodSpy).toHaveBeenCalled()
    })

})

describe('GoTo()', () => {

    test('Passing the correct parameters', () => {
        expect(() => fakeSlider.goTo()).toThrowError(ErrorMessage.typeOf('index', 'number'))
    })

    test('Toggle the [aria-selected], [aria-hidden] and [disabled] attributes', () => {
        fakeSlider.goTo(0)
        expect(fakeSlider._current).toBe(0)
        expect(fakeSlider._slides[0].getAttribute('aria-hidden')).toBe('false')
        expect(fakeSlider._buttons.tabs[0].getAttribute('aria-selected')).toBe('true')
        expect(fakeSlider._buttons.prev.hasAttribute('disabled')).toBeTruthy()
        fakeSlider.goTo(1)
        expect(fakeSlider._current).toBe(1)
        expect(fakeSlider._slides[0].getAttribute('aria-hidden')).toBe('true')
        expect(fakeSlider._slides[1].getAttribute('aria-hidden')).toBe('false')
        expect(fakeSlider._buttons.tabs[0].getAttribute('aria-selected')).toBe('false')
        expect(fakeSlider._buttons.tabs[1].getAttribute('aria-selected')).toBe('true')
        expect(fakeSlider._buttons.prev.hasAttribute('disabled')).toBeFalsy()
        fakeSlider.goTo(2)
        expect(fakeSlider._current).toBe(2)
        expect(fakeSlider._slides[1].getAttribute('aria-hidden')).toBe('true')
        expect(fakeSlider._slides[2].getAttribute('aria-hidden')).toBe('false')
        expect(fakeSlider._buttons.tabs[1].getAttribute('aria-selected')).toBe('false')
        expect(fakeSlider._buttons.tabs[2].getAttribute('aria-selected')).toBe('true')
        expect(fakeSlider._buttons.next.hasAttribute('disabled')).toBeTruthy()
    })

    test('Go to the first or last slide via toGo() method', () => {

        const scrollSpy = vi.spyOn(fakeDivLoop, 'scroll')

        fakeSliderLoop.goTo(-1)

        expect(scrollSpy).toHaveBeenCalledWith({
            behavior: "smooth",
            left: 0,
        }, undefined)

        fakeSliderLoop.goTo(3)

        expect(scrollSpy).toHaveBeenCalledWith({
            behavior: "smooth",
            left: 400,
        }, undefined)

    })

    test('Emmit the slider:changed events', () => {
        const eventSpy = vi.spyOn(fakeSlider, 'emmitEvent')
        fakeSlider.goTo(0)
        expect(eventSpy).toHaveBeenCalledWith('changed', { current: 0, previous: 2 })
    })

})

describe('Prev()', () => {

    afterAll(() => fakeSlider.goTo(0))

    test('Emit a goTo() method with correct index', () => {
        fakeSlider.goTo(1)
        const methodSpy = vi.spyOn(fakeSlider, 'goTo')
        fakeSlider.prev()
        expect(methodSpy).toHaveBeenCalledWith(0)
    })

    test('Don\'t emmit the goTo() method if already first slide', () => {
        const methodSpy = vi.spyOn(fakeSlider, 'goTo')
        fakeSlider._current = 0
        fakeSlider.prev()
        expect(methodSpy).not.toHaveBeenCalled()
    })

})

describe('Next()', () => {

    afterAll(() => fakeSlider.goTo(0))

    test('Emit a goTo() method and index is X', () => {
        const methodSpy = vi.spyOn(fakeSlider, 'goTo')
        fakeSlider.next()
        expect(methodSpy).toHaveBeenCalledWith(1)
    })

    test('Don\'t emmit the goTo() method if already last slide', () => {
        const methodSpy = vi.spyOn(fakeSlider, 'goTo')
        fakeSlider._current = 2
        fakeSlider.next()
        expect(methodSpy).not.toHaveBeenCalled()
    })

})