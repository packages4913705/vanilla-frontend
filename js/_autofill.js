/**
 * ------------------------------------------------------------------
 * Auto fill
 * ------------------------------------------------------------------
 * This class enable the functionality to automatic filling some fields from another
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from "./utilities/_error"

export default class Autofill extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'autofill')

        // Check for errors
        if (el.tagName !== 'SELECT' && el.tagName !== 'INPUT') throw new Error(ErrorMessage.typeOf('el', 'input|select'))
        if (!el.hasAttribute('aria-controls')) throw new Error(ErrorMessage.withAttribute('el', 'aria-controls'))
        if (el.tagName === 'INPUT') {
            if (el.type !== 'text' && el.type !== 'file') throw new Error(ErrorMessage.typeOf('type', 'text|file'))
            if (el.type === 'text' && !el.hasAttribute('list')) throw new Error(ErrorMessage.withAttribute('input', 'list'))
        }

        // Define the properties
        this._type = el.tagName === 'SELECT' ? 'select' : el.type === 'file' ? 'file' : 'datalist'

        this._fields = el.hasAttribute('aria-controls') ? el.getAttribute('aria-controls').split(' ').filter(id => document.getElementById(id)).map(id => document.getElementById(id)) : []

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // CHANGE the element
        this._element.addEventListener('change', () => {

            switch (this._type) {
                case 'select':
                    this.#byOption(this._element.options[this._element.selectedIndex])
                    break
                case 'datalist':
                    this.#byOption(document.querySelector(`#${this._element.getAttribute('list')} [value="${this._element.value}"]`))
                    break
                case 'file':
                    this.#byFile(this._element.files[0])
                    break
            }

        })

    }

    /**
     * Set value from a <option>
     * 
     * @param {string} option - Current option selected
     * @private
     */
    #byOption(option) {

        this._fields.forEach(field => {
            const attribute = `data-${field.getAttribute('data-autofill')}`
            field.value = option && option.hasAttribute(attribute) ? option.getAttribute(attribute) : ''
        })

        // Emmit event
        this.emmitEvent('changed', { current: option })

    }

    /**
     * Set value from an uploaded file
     * 
     * @param {File} file - Current file selected
     * @private
     */
    #byFile(file) {

        // Clean data
        const extension = file.name.split('.').pop()
        const name = file.name.replace(`.${extension}`, '')

        this._fields.forEach(field => {

            // Define attribute
            const attribute = field.getAttribute('data-autofill')

            // Define the value
            let value
            switch (attribute) {
                case 'filename':
                    value = name
                    break
                case 'extension':
                    value = extension
                    break
                default:
                    value = file[attribute] ?? ''
            }

            // Set the value
            field.value = value

        })

        // Emmit event
        this.emmitEvent('changed', { current: file })

    }

}