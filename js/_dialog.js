/**
 * ------------------------------------------------------------------
 * Dialog
 * ------------------------------------------------------------------
 * This class enable the functionalities for open/close/submit an HTML dialog
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from './utilities/_error'

export default class Dialog extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'dialog')

        // Define the properties
        this._isModal = Boolean(this._element.getAttribute('aria-modal'))

        this._buttons = {
            open: document.querySelectorAll(`[aria-controls=${this._element.id}]`) ?? [],
            close: this._element.querySelectorAll('[data-dialog-close]') ?? [],
            submit: this._element.querySelectorAll('[type=submit]') ?? [],
        }

        this._form = this._element.querySelector('form') ?? {}

        this._previousFocus = null

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // OPEN
        this._buttons.open.forEach((button) => button.addEventListener('click', (e) => this.open(e)))

        // CLOSE
        this._buttons.close.forEach((button) => button.addEventListener('click', () => this.close()))

        // SUBMIT
        this._buttons.submit.forEach((button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault()
                this.submit()
            })
        })

    }

    /**
     * Remove the [inert] attribute from the body if the dialog a modal
     *
     * @private
     */
    #toggleBodyAttribute() {
        if (!this._isModal) return
        const numberOfDialogOpen = document.querySelectorAll('dialog[open][aria-modal=true]').length
        numberOfDialogOpen ? document.body.setAttribute('inert', true) : document.body.removeAttribute('inert')
    }

    /**
     * Open the dialog
     *
     */
    open(e) {

        // Run event before opening, and stop if e.preventDefault()
        const callback = this.emmitEvent('opening')
        if (callback.defaultPrevented) return

        // Open the dialog (the methode changed if it's a modal)
        this._isModal ? this._element.showModal() : this._element.show()

        // If open via an HTML element, keep it as previous to reset the focus
        if (e && e.target instanceof HTMLElement) {
            this._previousFocus = e.target
        }

        // Toggle the [inert] attribute on the body
        this.#toggleBodyAttribute()

        // Run event after opened
        this.emmitEvent('opened')

    }

    /**
     * Close the dialog
     *
     */
    close() {

        // Run event before closing, and stop if e.preventDefault()
        const callback = this.emmitEvent('closing')
        if (callback.defaultPrevented) return

        // Close the dialog
        this._element.close()

        // Toggle the [inert] attribute on the body
        this.#toggleBodyAttribute()

        // Reset the focus on the previous element
        if (this._previousFocus) this._previousFocus.focus()

        // Run event after closed
        this.emmitEvent('closed')

    }

    /**
     * Submit the form in the dialog as default
     *
     */
    submit() {

        // Check for errors
        if (!this._form.length) throw new Error(ErrorMessage.exist('form'))

        // Stop if the form is invalid (Default required fields)
        if (!this._form.reportValidity()) return

        // Add [aria-busy] attribute on each submit button
        this._buttons.submit.forEach((button) => button.setAttribute('aria-busy', true))

        // Run event before submitting, and stop if e.preventDefault()
        const callback = this.emmitEvent('submiting', { form: this._form })
        if (callback.defaultPrevented) return

        // Define if the form is submited by Javascript or default
        if (this._form.getAttribute('method') == 'dialog') {

            // Remove [aria-busy] attribute on each submit button
            this._buttons.submit.forEach((button) => button.removeAttribute('aria-busy', true))

            // Close the dialog element
            this._element.close()

            // Toggle the [inert] attribute on the body
            this.#toggleBodyAttribute()

            // Reset the focus on the previous element
            if (this._previousFocus) this._previousFocus.focus()

            // Run event after submited
            this.emmitEvent('submited', { form: this._form })

        } else {

            // Default <form> submit
            this._form.submit()

        }

    }

}