/**
 * ------------------------------------------------------------------
 * Sortable
 * ------------------------------------------------------------------
 * This class enable the functionality to sort a list or a table
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from './utilities/_error'

export default class Sortable extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'sortable')

        // Define the properties
        this._withHandle = this._element.querySelector('[data-handle=sortable]') ? true : false
        this._current = null

        // Bind this only one time foreach methods
        // * Because I add and remove events
        this.handleMouseDown = this.handleMouseDown.bind(this)
        this.handleMouseUp = this.handleMouseUp.bind(this)
        this.drag = this.drag.bind(this)
        this.dragging = this.dragging.bind(this)
        this.drop = this.drop.bind(this)

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Prevent default animation at the drop
        this._element.addEventListener('dragover', (e) => e.preventDefault())

        // Init the events on the items
        this.#initEvents()

    }

    /**
     * Get the list of the items
     * This is used for the emmitEvent
     * 
     * @return {object}
     */
    get _items() {
        return this._element.querySelectorAll('[draggable]')
    }


    /**
     * Init the items and the event listeners
     * 
     * @private
     */
    #initEvents() {

        // Add the events
        this._items.forEach(item => {
            item.addEventListener('mousedown', this.handleMouseDown)
            item.addEventListener('mouseup', this.handleMouseUp)
            item.addEventListener('dragstart', this.drag)
            item.addEventListener('dragenter', this.dragging)
            item.addEventListener('dragend', this.drop)
        })
    }

    /**
     * Reset the event listeners
     * 
     * @private
     */
    resetEvents() {

        // Remove the listeners
        this._items.forEach((item) => {
            item.removeEventListener('mousedown', this.handleMouseDown)
            item.removeEventListener('mouseup', this.handleMouseUp)
            item.removeEventListener('dragstart', this.drag)
            item.removeEventListener('dragenter', this.dragging)
            item.removeEventListener('dragend', this.drop)
        })

        // Re-initialise the items
        this.#initEvents()
    }

    /**
     * Handle the mousedown event
     * 
     * @private
     */
    handleMouseDown(e) {
        // Avoid if not an element
        if (!(e.target instanceof Element)) return
        const target = e.target.closest('[draggable]')
        if (!this._withHandle || (e.target.hasAttribute('data-handle') && e.target.getAttribute('data-handle') === 'sortable')) target.draggable = true
    }

    /**
     * Handle the mouseup event
     * 
     * @private
     */
    handleMouseUp(e) {
        // Avoid if not an element
        if (!(e.target instanceof Element)) return
        const target = e.target.closest('[draggable]')
        target.draggable = false
    }

    /**
     * Drag an item
     * 
     * @private
     */
    drag(e) {

        // Avoid if not an element
        if (!(e.target instanceof Element)) return

        // Get the item
        // * Bug with event listeners reset if passing some data
        const item = e.target.closest('[draggable]')

        // Check for errors
        if (!(item instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('item', 'HTMLElement'))

        // Prevent drag on text selection
        if (!item.draggable) return

        // Set the current property
        this._current = item

        // Add the attribute on the current
        item.setAttribute('aria-grabbed', true)

        // Run custom event
        this.emmitEvent('drag', { items: this._items, current: item })

    }

    /**
     * When dragging an item, move it before or after an element
     * 
     * @private
     */
    dragging(e) {

        // Avoid if not an element
        if (!(e.target instanceof Element)) return

        // Get the item
        // * Bug with event listeners reset if passing some data
        const item = e.target.closest('[draggable]')

        // Check for errors
        if (!(item instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('item', 'HTMLElement'))

        // Do nothing if there is NO current
        if (!this._current) return

        // Move the item
        const position = item === this._element.firstElementChild ? 'beforebegin' : 'afterend'
        item.insertAdjacentElement(position, this._current)

    }

    /**
     * Drop an item
     * 
     * @private
     */
    drop(e) {

        // Avoid if not an element
        if (!(e.target instanceof Element)) return

        // Get the item
        // * Bug with event listeners reset if passing some data
        const item = e.target.closest('[draggable]')

        // Check for errors
        if (!(item instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('item', 'HTMLElement'))

        // Do nothing if there is NO current
        if (!this._current) return

        // Reset the current property
        this._current = null

        // Remove the attribute on the current
        item.removeAttribute('aria-grabbed', true)

        // Reset draggable attribute
        item.draggable = false

        // Run custom event
        this.emmitEvent('drop', { items: this._items, current: item })

    }

}