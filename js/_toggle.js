/**
 * ------------------------------------------------------------------
 * Toggle
 * ------------------------------------------------------------------
 * This class enable the functionality to un/collapse some element by another
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 * 
 * * NOTE: Check availability of the ariaControlsElements to select the #ids
 */

import BaseComponent from './utilities/_base-component'

export default class Toggle extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'toggle')

        // Define the properties
        this._type = this._element.tagName == 'SELECT' ? 'select' : this._element.getAttribute('type') ?? 'button'

        const arialist = this._element.hasAttribute('aria-controls') ? this._element.getAttribute('aria-controls').split(' ').filter(id => document.getElementById(id)) : []
        this._toggables = arialist.map(id => document.getElementById(id))

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // On first load run method toggle()
        this.toggle()

        if (this._type === 'button') {

            // CLICK for <button> and change the [aria-pressed] attribute
            this._element.addEventListener('click', () => {
                this._element.setAttribute('aria-pressed', this.value === 'false')
                this.toggle()
            })

        } else {

            // CHANGE for <checkbox> <radio> <select>
            this._element.addEventListener('change', () => this.toggle())

        }

    }

    /**
     * Define the value: <checkbox> and <radio> = :checked, <button> = [aria-pressed], <else> = value
     * 
     * @return {string}
     */
    get value() {

        switch (this._type) {
            case 'button':
                return (this._element.getAttribute('aria-pressed') === 'true').toString()

            case 'checkbox':
            case 'radio':
                return this._element.checked.toString()

            default:
                return this._element.value
        }

    }

    /**
     * Toggle the [hidden] and [aria-expanded] attributes
     * 
     */
    toggle() {

        // Define the group value for <select>
        const groupValue = this._type === 'select' && this._element.querySelector('option:checked') ? this._element.querySelector('option:checked').parentElement.label ?? null : null

        // Check for each toggables elements
        this._toggables.forEach(toggable => {

            // Define if there is a [data-toggle-when] attribute, otherwise check on the true value
            const toggleValue = toggable.getAttribute('data-toggle-when') ?? 'true'

            // Toggle the [hidden] attribute
            toggable.hidden = this.value !== toggleValue && groupValue !== toggleValue

            if (!toggable.hidden) {
                const focus = toggable.getAttribute('tabindex') === '0' ? toggable : toggable.querySelector('[tabindex="0"]') ?? toggable.querySelector('button, a, input')
                if (focus) focus.focus()
            }

        })

        // Update the [aria-expanded] attribute if there is any toggables visible
        const areAllHidden = this._toggables.every(el => el.hidden)
        this._element.setAttribute('aria-expanded', !areAllHidden)

        // If the _element is <radio>, emmit event 'change' on <radio> with same [name]
        if (this._type === 'radio' && this._element.checked) document.querySelectorAll(`input[name="${this._element.name}"]:not(:checked)`).forEach(radio => radio.dispatchEvent(new Event('change')))

        // Emmit event
        this.emmitEvent('changed')

    }

}