/**
 * ------------------------------------------------------------------
 * Comfort
 * ------------------------------------------------------------------
 * This class create and manage the cookies for saving theme and style changes for visual comfort
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import Cookie from "./utilities/_cookie"
import ErrorMessage from "./utilities/_error"

export default class Comfort extends Cookie {

    /**
     * Creates an instance
     *
     * @param {string} name - The name of the cookie
     * @constructor
     */
    constructor(name = '_comfort') {

        // Run the SUPER constructor from Cookie
        super(name)

        // Define the properties
        this._buttons = {
            theme: document.querySelectorAll('body [data-theme]'),
            style: document.querySelectorAll('body [data-style]'),
        }

        // Init the cookie
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // If the cookie is NOT empty, toggle the attributes on <html> <body> and <button>
        if (Object.keys(this.value).length !== 0) {
            this.#toggleTheme()
            this.#toggleStyle()
        }

        // CLICK on theme buttons
        this._buttons.theme.forEach((button) => button.addEventListener('click', () => this.setTheme(button.value)))

        // CLICK on style buttons
        this._buttons.style.forEach((button) => button.addEventListener('click', () => this.setStyle(button.getAttribute('data-style'), button.value)))

    }

    /**
     * Toggle the [data-theme] attribute on the <html>
     * and the [aria-pressed] attribute on the <button>
     * 
     * @private
     */
    #toggleTheme() {

        // Do nothing if there is no theme in the cookie
        if (!this.has('theme')) return

        // Set the [data-theme] attribute on the <html>
        document.documentElement.setAttribute('data-theme', this.get('theme'))

        // Set the [aria-pressed] attribute on the <button>
        this._buttons.theme.forEach(button => button.setAttribute('aria-pressed', button.value === this.get('theme')))

    }

    /**
     * Toggle the [style] attribute on the <body>
     * and the [aria-pressed] attribute on the <button>
     * 
     * @private
     */
    #toggleStyle() {

        // Get the style object stored in cookie
        const style = this.get('style')

        // Do nothing if there is no style in the cookie
        if (!style || style.length === 0) return

        // Set the [style] attribute on the <body>
        const styleAsString = Object.entries(style).map(([k, v]) => `${k}:${v}`).join(';')
        document.body.setAttribute('style', styleAsString)

        // Set the [aria-pressed] attribute on the <button>
        this._buttons.style.forEach(button => {
            const attribute = button.getAttribute('data-style')
            button.setAttribute('aria-pressed', style.hasOwnProperty(attribute) && style[attribute] === button.value)
        })

    }

    /**
     * Save the theme inside the cookie
     * 
     * @param {string} value - Name of the theme
     */
    setTheme(value) {

        // Check for errors
        if (typeof value !== 'string') throw new Error(ErrorMessage.typeOf('value', 'string'))

        // Do nothing if it's already the current theme
        if (this.get('theme') === value) return

        // Save the cookie
        this.set({ ...this.value, theme: value })

        // Then toggle theme
        this.#toggleTheme()

    }

    /**
     * Save the style inside the cookie
     * 
     * @param {string} name - Name of the CSS property
     * @param {string} value - Value of the CSS property
     */
    setStyle(name, value) {

        // Check for errors
        if (typeof name !== 'string') throw new Error(ErrorMessage.typeOf('name', 'string'))
        if (typeof value !== 'string') throw new Error(ErrorMessage.typeOf('value', 'string'))

        // Get the style inside the cookie
        const style = this.get('style')

        // Do nothing if it's already the current style
        if (style && style.hasOwnProperty(name) && style[name] === value) return

        // Save the cookie
        this.set({
            ...this.value,
            style: { ...this.value.style, ...{ [name]: value } }
        })

        // Then toggle style
        this.#toggleStyle()

    }

    /**
     * Reset the comfort cookie
     * 
     */
    reset() {

        // Delete the cookie
        this.delete()

        // Remove the attributes on <html> <body> and <button>
        document.documentElement.removeAttribute('data-theme')
        document.body.removeAttribute('style');

        [...this._buttons.theme, ...this._buttons.style].forEach(button => {
            button.removeAttribute('aria-pressed')
        })

    }

}