/**
 * ------------------------------------------------------------------
 * Drawer
 * ------------------------------------------------------------------
 * This class enable the functionality to toggles a drawer to show/hide some content
 * Most used case is a sidebar or a main menu to hide on mobile
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 * 
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from "./utilities/_error"
import Cookie from "./utilities/_cookie"

export default class Drawer extends BaseComponent {

    static OPTIONS = {
        breakpoint: 960,
        cookie: false
    }

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        if (options.breakpoint && typeof options.breakpoint !== 'number') throw new Error(ErrorMessage.typeOf('options.breakpoint', 'number'))
        if (options.cookie && typeof options.cookie !== 'string') throw new Error(ErrorMessage.typeOf('options.cookie', 'string'))

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'drawer')

        // Define the properties
        this._buttons = document.querySelectorAll(`[aria-controls=${this._element.id}]`) ?? []

        this._backdrop = document.getElementById('backdrop') ?? null

        this._timeout = false

        this._isOpen = !this._element.hidden

        this._focus = this._element.getAttribute('tabindex') === '0' ? this._element : this._element.querySelector('[tabindex="0"]') ?? this._element.querySelector('button, a, input')

        this._cookie = this._options.cookie ? new Cookie(this._options.cookie) : null

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Set the cookie by default
        if (this._cookie && !this._cookie.has('drawer-is-open')) this._cookie.set({ ...this._cookie.value, 'drawer-is-open': this._isOpen })

        // Define the default state on bigger screen
        const shouldBeOpen = this._cookie ? this._cookie.get('drawer-is-open') : true

        // Window is bigger than breakpoint
        // -> Show/Hide the drawer by the cookie
        if (window.innerWidth > this._options.breakpoint) this.toggle(shouldBeOpen)

        // Window is smaller than breakpoint
        // -> Hide the drawer
        if (window.innerWidth <= this._options.breakpoint) this.toggle(false)

        // On window resize
        // -> Toggle the drawer
        window.onresize = () => {
            clearTimeout(this._timeout)
            this._timeout = setTimeout(() => {

                // Bigger than breakpoint
                if (window.innerWidth > this._options.breakpoint && (!this._isOpen && shouldBeOpen)) this.toggle(true)

                // Smaller than breakpoint -> Invisible
                if (window.innerWidth <= this._options.breakpoint && this._isOpen) this.toggle(false)

            }, 250)
        }

        // On click on the button toggle the drawer
        this._buttons.forEach((button) => button.addEventListener('click', () => this.toggle()))

        // On click on the backdrop, close the drawer
        if (this._backdrop) this._backdrop.addEventListener('click', () => this.toggle())

    }

    /**
     * Toggle the drawer
     * 
     * @param {boolean} value 
     */
    toggle(value = !this._isOpen) {

        // Change the state
        this._isOpen = value

        // Change the [aria-pressed] & [aria-expanded] attribute on the <button>
        this._buttons.forEach((button) => {
            button.setAttribute('aria-pressed', this._isOpen)
            button.setAttribute('aria-expanded', this._isOpen)
        })

        // Change the [hidden] attribute on the drawer
        this._element.hidden = !this._isOpen

        // Change the cookie
        if (this._cookie && (this._cookie.get('drawer-is-open') !== this._isOpen) && window.innerWidth > this._options.breakpoint) {
            this._cookie.set({ ...this._cookie.value, 'drawer-is-open': this._isOpen })
        }

        // Add the focus if open
        // * Need to wait the transition before make it focused
        if (this._focus && this._isOpen) {
            this._element.addEventListener("transitionend", () => {
                this._focus.focus()
            }, { once: true })
        }

    }


}