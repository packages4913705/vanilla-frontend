/**
 * ------------------------------------------------------------------
 * Slider
 * ------------------------------------------------------------------
 * This class enables the functionality to make an element slider
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 * 
 * Keep check on the scrollend event: https://caniuse.com/?search=scrollend
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from "./utilities/_error"

export default class Slider extends BaseComponent {

    static OPTIONS = {
        behavior: 'smooth', // Can be auto, smooth, or instant
        loop: false,
        autoplay: false
    }

    /**
     * Creates an instance of Slider.
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - Custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Check for errors in the options
        if (options.behavior && !['auto', 'smooth', 'instant'].includes(options.behavior)) {
            throw new Error(ErrorMessage.enumOf('options.behavior', 'auto|smooth|instant'))
        }
        if (options.loop && typeof options.loop !== 'boolean') {
            throw new Error(ErrorMessage.typeOf('options.loop', 'boolean'))
        }
        if (options.autoplay && (typeof options.autoplay !== 'boolean' && typeof options.autoplay !== 'number')) {
            throw new Error(ErrorMessage.typeOf('options.autoplay', 'boolean|number'))
        }

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'slider')

        // Handle reduced motion preferences
        const isReduced = window.matchMedia('(prefers-reduced-motion: reduce)').matches
        if (isReduced) this._options.behavior = 'instant'

        // Get slides and buttons
        this._slides = Array.from(this._element.querySelectorAll('[role=tabpanel]'))

        this._buttons = {
            prev: document.querySelector(`[aria-controls=${this._element.id}][data-slider-prev]`),
            next: document.querySelector(`[aria-controls=${this._element.id}][data-slider-next]`),
            tabs: document.querySelectorAll(`[aria-controls=${this._element.id}][role=tablist] [role=tab]`)
        }

        this._current = 0
        this._interval = null

        // Clone first and last slides if loop is enabled
        if (this._options.loop && this._options.behavior == 'smooth') {

            const cloneFirst = this._element.firstElementChild.cloneNode(true)
            const cloneLast = this._element.lastElementChild.cloneNode(true)

            cloneFirst.removeAttribute('id')
            cloneFirst.removeAttribute('aria-hidden')
            cloneFirst.removeAttribute('role')
            this._element.append(cloneFirst)

            cloneLast.removeAttribute('id')
            cloneLast.removeAttribute('aria-hidden')
            cloneLast.removeAttribute('role')
            this._element.prepend(cloneLast)

        }

        // Initialize event listeners and functionality
        this.#init()
    }

    /**
     * Initialize event listeners and autoplay functionality.
     * 
     * @private
     */
    #init() {

        // Handle autoplay
        if (this._options.autoplay) {
            this._interval = setInterval(() => this.next(), this._options.autoplay)
        }

        // Add event listeners for next and prev buttons
        if (this._buttons.next) this._buttons.next.addEventListener('click', () => this.next())
        if (this._buttons.prev) this._buttons.prev.addEventListener('click', () => this.prev())

        // Add event listeners for tabs
        if (this._buttons.tabs.length) {
            this._buttons.tabs.forEach((tab, index) => tab.addEventListener('click', () => this.goTo(index)))
        }

        // Handle autoplay reset after user interaction
        if (this._options.autoplay) {
            this._element.addEventListener('click', () => {
                clearInterval(this._interval)
                this._interval = setInterval(() => this.next(), this._options.autoplay)
            })
        }

        // Enable swipe functionality on touch devices.
        if (!this._options.autoplay) {
            let startX = 0

            const onTouchStart = (event) => {
                startX = event.touches[0].clientX
            }

            const onTouchEnd = (event) => {
                const endX = event.changedTouches[0].clientX
                if (startX > endX + 50) {
                    this.next()
                } else if (startX < endX - 50) {
                    this.prev()
                }
            }

            this._element.addEventListener('touchstart', onTouchStart)
            this._element.addEventListener('touchend', onTouchEnd)
        }
    }

    /**
     * Go to a specific slide by index.
     * 
     * @param {int} index - The index of the slide
     */
    goTo(index) {

        if (typeof index !== 'number') {
            throw new Error(ErrorMessage.typeOf('index', 'number'))
        }

        const previous = this._current

        let clone

        if (this._options.loop && (index < 0 || index >= this._slides.length)) {

            // Go to the correct slide index
            this._current = index < 0 ? this._slides.length - 1 : 0

            // If scroll behavior go to the clone offset
            if (this._options.behavior == 'smooth') {
                clone = this._current == 0 ? this._element.lastElementChild : this._element.firstElementChild
            }

        } else {
            // Otherwise go to the index
            this._current = index
        }

        // Define the offset
        const offset = clone ? clone.offsetLeft : this._slides[this._current].offsetLeft

        // Go to the slide
        this._element.scrollTo({
            left: offset,
            behavior: this._options.behavior
        })

        // If clone offset, then go to the right slide instant
        if (clone) {
            setTimeout(() => {
                this._element.scrollTo({
                    left: this._slides[this._current].offsetLeft,
                    behavior: 'instant'
                })
            }, 500)
        }

        // Update the [aria-selected] attribute on tabs
        this._buttons.tabs.forEach((tab, idx) => tab.setAttribute('aria-selected', idx === this._current))

        // Update the [aria-hidden] attribute on slides
        this._slides.forEach((slide, idx) => slide.setAttribute('aria-hidden', idx !== this._current))

        // Disable next and prev buttons if necessary
        if (this._buttons.next && !this._options.loop) {
            this._buttons.next.disabled = this._current === this._slides.length - 1
        }
        if (this._buttons.prev && !this._options.loop) {
            this._buttons.prev.disabled = this._current === 0
        }

        // Emit "changed" event after the slide has changed
        this.emmitEvent('changed', { current: this._current, previous: previous })
    }

    /**
     * Go to the next slide.
     */
    next() {
        const nextIndex = this._current + 1

        // Prevent scrolling if at the last slide and looping is disabled
        if (!this._options.loop && nextIndex >= this._slides.length) return

        // Move to the next slide
        this.goTo(nextIndex)
    }

    /**
     * Go to the previous slide.
     */
    prev() {
        const prevIndex = this._current - 1

        // Prevent scrolling if at the first slide and looping is disabled
        if (!this._options.loop && prevIndex < 0) return

        // Move to the previous slide
        this.goTo(prevIndex)
    }

}
