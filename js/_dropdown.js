/**
 * ------------------------------------------------------------------
 * Dropdown
 * ------------------------------------------------------------------
 * This class enable the functionality to un/collapse some element by another
 * This component is a simpler version of the Toggle
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 * 
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from './utilities/_error'

export default class Dropdown extends BaseComponent {

    static OPTIONS = {
        closable: true
    }

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Check for errors
        if (options.closable && typeof options.closable !== 'boolean') throw new Error(ErrorMessage.typeOf('options.closable', 'boolean'))

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'dropdown')

        // Define the properties
        this._button = this._element.querySelector('[aria-controls]')
        this._content = document.getElementById(this._button.getAttribute('aria-controls'))
        this._focus = this._content.getAttribute('tabindex') === '0' ? this._content : this._content.querySelector('[tabindex="0"]') ?? this._content.querySelector('button, a, input')

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Toggle the content on click on the button
        this._button.addEventListener('click', () => this.toggle())

        // Close the content is click outside
        if (this._options.closable) {
            this._content.addEventListener('focusout', (e) => {
                if (e.relatedTarget !== this._button && !this._content.contains(e.relatedTarget)) this.toggle()
            })
        }

    }

    /**
     * Define the value
     * 
     * @return {string}
     * @private
     */
    get value() {
        return this._button.getAttribute('aria-pressed') === 'true'
    }

    /**
     * Toggle the [hidden] and [aria-expanded] attributes
     * 
     */
    toggle() {

        // Define new value
        const value = !this.value

        // Change the [aria-pressed] and [aria-expanded] attribute on button
        this._button.setAttribute('aria-pressed', value)
        this._button.setAttribute('aria-expanded', value)

        // Change visibility of the content
        this._content.hidden = !value

        // Add the focus if open
        // * Need to wait the transition before make it focused
        if (this._focus && value) {
            this._element.addEventListener("transitionend", () => {
                this._focus.focus()
            }, { once: true })
        }

        // Emmit event
        this.emmitEvent('changed', { isOpen: !this._content.hidden })

    }

}