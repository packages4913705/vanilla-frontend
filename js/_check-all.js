/**
 * ------------------------------------------------------------------
 * Check all
 * ------------------------------------------------------------------
 * This class enable the functionality to un/check a list of checkboxes
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from './utilities/_base-component'

export default class CheckAll extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'checkall')

        // Define the properties
        this._checkboxes = document.querySelectorAll(`input[type="checkbox"][name="${this._element.value}"]`)

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Check the "check all" checkbox by default is needed
        this._element.checked = this.number.total === this.number.checked

        // CLICK on "check all"
        this._element.addEventListener('change', () => {
            this._checkboxes.forEach((checkbox) => checkbox.checked = this._element.checked)
        })

        // CLICK on "checkboxes"
        this._checkboxes.forEach(checkbox => {
            checkbox.addEventListener('change', () => this._element.checked = this.number.total === this.number.checked)
        })

    }

    /**
     * Get the number of checkboxes as total, checked and unchecked
     * 
     * @returns {object}
     */
    get number() {

        let checkboxes = {
            total: this._checkboxes.length,
            checked: 0,
            unchecked: 0
        }

        this._checkboxes.forEach((checkbox) => checkbox.checked ? checkboxes.checked++ : checkboxes.unchecked++)

        return checkboxes

    }

}