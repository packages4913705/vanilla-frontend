/**
 * ------------------------------------------------------------------
 * Consent
 * ------------------------------------------------------------------
 * This class create and manage the cookies for saving the consent to comply the GDPR
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import Cookie from "./utilities/_cookie"
import Dialog from "./_dialog"
import ErrorMessage from "./utilities/_error"

export default class Consent extends Cookie {

    static DEFAULT = { necessary: true }

    /**
     * Creates an instance
     *
     * @param {string} name - The name of the cookie
     * @param {string} dialogID - The id of the cookies dialog
     * @constructor
     */
    constructor(name = '_consent', dialogID = 'cookies') {

        // Check for errors
        if (typeof dialogID !== 'string') throw new Error(ErrorMessage.typeOf('dialogID', 'string'))
        if (!document.getElementById(dialogID)) throw new Error(ErrorMessage.existById('dialog', dialogID))

        // Run the SUPER constructor from Cookie
        super(name)

        // Define the properties
        this._dialog = new Dialog(document.getElementById(dialogID))

        // Init the cookie
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // If the cookie is empty, open the dialog
        if (Object.keys(this.value).length === 0) this._dialog.open()

        // Set the cookie via the dialog:closed event
        this._dialog._element.addEventListener('dialog:closed', () => this.set(Consent.DEFAULT))

        // Set the cookie via the dialog:submiting event
        this._dialog._element.addEventListener('dialog:submiting', (e) => this.#setByForm(e.detail.form))

    }

    /**
     * Save the consent cookie via a form with input of type checkbox
     * 
     * @param {HTMLElement} form 
     * @private
     */
    #setByForm(form) {

        // Check for errors
        if (!(form instanceof HTMLElement)) throw new Error('Whoops, the form parameter must be an HTML Element !')

        // Get all the input of type checkbox with name cookies_consent[]
        const fields = form.querySelectorAll('input[type="checkbox"][name="cookies_consent[]"]')

        // If there is some fields, add each to the variable preferences and save it
        if (fields.length) {
            const preferences = [...fields].reduce((obj, field) => { return { ...obj, ...Consent.DEFAULT, [field.value]: field.checked } }, {})
            this.set(preferences)
        }

    }

}