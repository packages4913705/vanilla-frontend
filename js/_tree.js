/**
 * ------------------------------------------------------------------
 * Tree
 * ------------------------------------------------------------------
 * This class enable the functionality to expand/contract element as a tree
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from './utilities/_base-component'

export default class Tree extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'tree')

        // Define the properties
        this._type = this._element.tagName == 'TABLE' ? 'grid' : 'list'

        this._withHandle = this._type == 'grid' && this._element.querySelector('[data-handle=tree]') ? true : false

        // Bind this only one time foreach methods
        // * Because I add and remove events
        this.toggle = this.toggle.bind(this)

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Init the events on the items
        this.#initEvents()

    }

    /**
     * Get the list of the items
     * 
     * @return {object}
     */
    get _items() {
        return this._element.querySelectorAll('[aria-expanded]')
    }

    /**
     * Init the items and the event listeners
     * 
     * @private
     */
    #initEvents() {
        this._items.forEach(item => {
            item.addEventListener('click', this.toggle)
        })
    }

    /**
     * Reset the event listeners
     * 
     * @private
     */
    resetEvents() {

        // Remove the listeners
        this._items.forEach((item) => {
            item.removeEventListener('click', this.toggle)
        })

        // Re-initialise the items
        this.#initEvents()

    }

    /**
     * Toggle the [hidden] and [aria-expanded] attributes
     * 
     * @param {HTMLElement} item - The current item
     */
    toggle(e) {

        let item

        if (e instanceof Event) {
            // If it's an event verify about the handle + if target is an HTML element
            if (!(e.target instanceof Element)) return
            if (this._withHandle && (!e.target.hasAttribute('data-handle') || e.target.getAttribute('data-handle') !== 'tree')) return
            e.stopPropagation()
            item = e.target.closest('[aria-expanded]')
        } else {
            // If it's NOT an event verify that it's an HTML element
            if (!(e instanceof Element)) return
            item = e
        }

        // Check for errors
        if (!(item instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('item', 'HTMLElement'))

        // Update the [aria-expanded]
        const isExpanded = item.getAttribute('aria-expanded') === 'true'
        item.setAttribute('aria-expanded', !isExpanded)

        // Get children to toggle
        const children = item.hasAttribute('aria-controls') ? item.getAttribute('aria-controls').split(' ').filter(id => document.getElementById(id)).map(id => document.getElementById(id)) : []

        // Toggle the [hidden] attribute on the children
        children.forEach(child => child.hidden = isExpanded)

        // Set the focus on the required child
        if (!isExpanded) {

            let childToFocus = null

            const child = children.find(el => {

                // Check if the child have a tabindex
                if (el.getAttribute('tabindex') === '0') {
                    childToFocus = el
                    return el
                }

                // Otherwise if an element inside the child have the tabindex
                const childWithTabIndex = el.querySelector('[tabindex="0"]')
                if (childWithTabIndex) {
                    childToFocus = childWithTabIndex
                    return childWithTabIndex
                }

                // Otherwise get the first focusable element
                childToFocus = el.querySelector('button, a, input')
                return el

            })

            // Add the focus if open
            // * Need to wait the transition before make it focused
            if (child && childToFocus) {
                childToFocus.focus()
            }

        }

        // If type grid, collapse the subchildren
        if (this._type === 'grid' && isExpanded) children.filter(child => child.hasAttribute('aria-expanded') && child.getAttribute('aria-expanded') === 'true').forEach(child => this.toggle(child))

        // Emmit event
        this.emmitEvent('changed', { isOpen: !isExpanded })

    }

}