/**
 * ------------------------------------------------------------------
 * Scroll
 * ------------------------------------------------------------------
 * This script enable an HTML element to scroll to a position and to spy some buttons
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import BaseComponent from "./utilities/_base-component"
import ErrorMessage from "./utilities/_error"

export default class Scroll extends BaseComponent {

    static OPTIONS = {
        behavior: 'smooth', // Can be auto, smooth or instant
        navigation: null, // Id of the navigation
        gaps: {
            top: 100,
            bottom: 100,
            spy: 100
        }
    }

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Check for errors
        if (options.gaps) {
            if (!(options.gaps instanceof Object)) throw new Error(ErrorMessage.instanceOf('options.gaps', 'Object'))
            if (options.gaps.top && typeof options.gaps.top !== 'number') throw new Error(ErrorMessage.typeOf('options.gaps.top', 'number'))
            if (options.gaps.bottom && typeof options.gaps.bottom !== 'number') throw new Error(ErrorMessage.typeOf('options.gaps.bottom', 'number'))
            if (options.gaps.spy && typeof options.gaps.spy !== 'number') throw new Error(ErrorMessage.typeOf('options.gaps.spy', 'number'))
        }
        if (options.behavior && !['auto', 'smooth', 'instant'].includes(options.behavior)) throw new Error(ErrorMessage.enumOf('options.behavior', 'auto|smooth|instant'))
        if (options.navigation && !document.getElementById(options.navigation)) throw new Error(ErrorMessage.existById('element', options.navigation))

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'scroll')

        // Reduce animation
        const isReduced = window.matchMedia(`(prefers-reduced-motion: reduce)`) === true || window.matchMedia(`(prefers-reduced-motion: reduce)`).matches === true
        if (isReduced) this._options.behavior = 'instant'

        // Define the properties
        this._buttons = {
            top: document.querySelector(`[data-scroll-top="${this._element.id}"]`) ?? null,
            bottom: document.querySelector(`[data-scroll-bottom="${this._element.id}"]`) ?? null
        }

        const links = [...document.querySelectorAll(`#${this._options.navigation} a[href^="#"]`)].filter((a) => document.getElementById(a.getAttribute('href').substring(1)))
        this._spy = {
            enable: this._options.navigation && links.length ? true : false,
            links: links,
            targets: links.map(a => document.getElementById(a.getAttribute('href').substring(1))),
            current: 0
        }

        // Init the event listener
        this.#init()

    }

    /**
     * Get the height of the element
     * It's a get method because the height can change by screen size, CSS and more !
     * 
     * @returns {int}
     */
    get _height() {
        return this._element.scrollHeight - this._element.clientHeight
    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // SCROLL
        if (this._element.nodeName === 'HTML') {
            window.onscroll = () => this.#scroll()
        } else {
            this._element.onscroll = () => this.#scroll()
        }

        // CLICK on links
        [...this._spy.links].forEach((link, index) => link.addEventListener('click', (e) => {
            e.preventDefault()
            this.goTo(this._spy.targets[index].offsetTop)
        }))

        // CLICK on buttons
        if (this._buttons.top) this._buttons.top.addEventListener('click', () => this.scrollTop())
        if (this._buttons.bottom) this._buttons.bottom.addEventListener('click', () => this.scrollBottom())

    }

    /**
     * Define what to do on scroll
     * 
     * @private
     */
    #scroll() {

        // If Spy, run the method #spy
        if (this._spy.enable) this.#spy()

        // Toggle the top button
        if (this._buttons.top) this._buttons.top.hidden = this._element.scrollTop <= this._options.gaps.top

        // Toggle the bottom button
        if (this._buttons.bottom) this._buttons.bottom.hidden = this._element.scrollTop >= this._height - this._options.gaps.bottom

    }

    /**
     * Spy if current section changed, and toggle the [aria-current] attribute on links
     * 
     * @private
     */
    #spy() {

        const current = this._spy.targets.length - [...this._spy.targets].reverse().findIndex((target) => this._element.scrollTop >= target.offsetTop - this._options.gaps.spy) - 1

        if (current !== this._spy.current) {

            // Remove [aria-current] on each links
            this._spy.links.forEach(link => link.removeAttribute('aria-current'))

            // Define current section
            this._spy.current = current

            // Set [aria-current] on link
            if (this._spy.links[this._spy.current]) this._spy.links[this._spy.current].setAttribute('aria-current', 'location')

        }

    }

    /**
     * Scroll to a certain distance
     * 
     * @param {number} distance 
     */
    goTo(distance) {

        // Check for errors
        if (typeof distance !== 'number') throw new Error(ErrorMessage.typeOf('distance', 'number'))

        // Scroll to
        this._element.scrollTo({
            top: distance,
            behavior: this._options.behavior
        })

    }

    /**
     * Scroll to the top
     * 
     */
    scrollTop() {
        this.goTo(0)
    }

    /**
     * Scroll to the bottom
     * 
     */
    scrollBottom() {
        this.goTo(this._height)
    }

} 