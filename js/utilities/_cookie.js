/**
 * ------------------------------------------------------------------
 * Cookie
 * ------------------------------------------------------------------
 * This class create and manage the cookies
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import ErrorMessage from "./_error"

export default class Cookie {

    #NAME

    /**
     * Creates an instance
     *
     * @param {string} name - The name of the cookie
     * @constructor
     */
    constructor(name) {

        // Check for errors
        if (typeof name !== 'string') throw new Error(ErrorMessage.typeOf('name', 'string'))

        // Define the static private properties
        this.#NAME = name

    }

    /**
     * Get the cookie value
     * Code from {@link https://www.w3schools.com/js/js_cookies.asp W3School}.
     * 
     * @returns {object}
     */
    get value() {

        const name = `${this.#NAME}=`
        let decodedCookie = decodeURIComponent(document.cookie)
        let ca = decodedCookie.split(';')
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i]
            while (c.charAt(0) == ' ') {
                c = c.substring(1)
            }
            if (c.indexOf(name) == 0) {
                return JSON.parse(c.substring(name.length, c.length))
            }
        }
        return {}

    }

    /**
     * Set the object value as string in the cookie
     * 
     * @param {object} value - Value of the cookie
     */
    set(value) {
        if (!(value instanceof Object)) throw new Error(ErrorMessage.instanceOf('value', 'Object'))
        document.cookie = `${this.#NAME}=${JSON.stringify(value)};SameSite=Lax;Path=/;`
    }

    /**
     * Check if the cookie value has a specific key
     * 
     * @param {string} key - The string to search
     * @returns {boolean}
     */
    has(key) {
        if (typeof key !== 'string') throw new Error(ErrorMessage.typeOf('key', 'string'))
        return this.value.hasOwnProperty(key)
    }

    /**
     * Get a specific cookie value per key
     * 
     * @param {string} key - The string to search
     * @returns
     */
    get(key) {
        if (typeof key !== 'string') throw new Error(ErrorMessage.typeOf('key', 'string'))
        return this.has(key) ? this.value[key] : null
    }

    /**
     * Delete the cookie
     * 
     */
    delete() {
        document.cookie = this.#NAME + "=;SameSite=Lax;Path=/; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    }

}