/**
 * ------------------------------------------------------------------
 * Base component
 * ------------------------------------------------------------------
 * This class regroup common functionalities for other classes
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import ErrorMessage from "./_error"

export default class BaseComponent {

    #NAME

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The element
     * @param {Object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}, name = this.constructor.name.toLowerCase()) {

        // Check for errors
        if (!(el instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('el', 'HTMLElement'))
        if (!(options instanceof Object)) throw new Error(ErrorMessage.instanceOf('options', 'Object'))

        // Define the #NAME private properties
        this.#NAME = name

        // Define the _element property
        this._element = el

        // Define the _options property by merging the options parameter with the static OPTIONS
        const defaultOptions = this.constructor.OPTIONS ?? {}
        this._options = this.mergeObject(defaultOptions, options)

    }

    /**
     * Merge two object to a single one (accept multi-level)
     * 
     * @param {Object} one 
     * @param {Object} two 
     * @returns {Object}
     */
    mergeObject(one, two) {

        // Check for errors
        if (!(one instanceof Object)) throw new Error(ErrorMessage.instanceOf('one', 'Object'))
        if (!(two instanceof Object)) throw new Error(ErrorMessage.instanceOf('two', 'Object'))

        // Reduce the objects in a single one
        return Object.keys(one).reduce((obj, key) => {

            // Define the final option (if key is an object => call the method )
            const option = two.hasOwnProperty(key) ? (one[key] instanceof Object ? this.mergeObject(one[key], two[key]) : two[key]) : one[key]

            // Merge option  
            return { ...obj, [key]: option }

        }, {})

    }

    /**
     * Create a CustomEvent
     * 
     * @param {String} name - The name of the event
     * @param {Object} data - The data to pass
     * @param {HTMLElement} selector - The HTML element who will have the event
     * @returns {CustomEvent}
     */
    emmitEvent(name, data = {}, selector = this._element) {

        // Check for errors
        if (typeof name !== 'string') throw new Error(ErrorMessage.typeOf('name', 'string'))
        if (!(data instanceof Object)) throw new Error(ErrorMessage.instanceOf('data', 'Object'))
        if (!(selector instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('selector', 'HTMLElement'))

        // Define the name of the event as classname:event
        const eventName = `${this.#NAME}:${name}`

        // Create a new CustomEvent
        const newEvent = new CustomEvent(eventName, {
            cancelable: true,
            detail: data,
        })

        // Dispatch the event on the HTMLElement
        selector.dispatchEvent(newEvent)

        // Return the CustomEvent
        return newEvent

    }

}