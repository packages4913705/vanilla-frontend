/**
 * ------------------------------------------------------------------
 * Error
 * ------------------------------------------------------------------
 * This make global error message
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

export default class ErrorMessage {

    /**
     * For: !(parameter instanceof type)
     * 
     * @param {string} parameter 
     * @param {string} type 
     * @returns {string}
     */
    static instanceOf(parameter, type) {
        return `Whoops, the "${parameter}" parameter must be an instance of ${type} !`
    }

    /**
     * For: typeof parameter !=== type
     * 
     * @param {string} parameter 
     * @param {string} type 
     * @returns {string}
     */
    static typeOf(parameter, type) {
        return `Whoops, the "${parameter}" parameter must be a ${type} !`
    }

    /**
     * For: ![values].includes(parameter)
     * 
     * @param {string} parameter 
     * @param {string} type 
     * @returns {string}
     */
    static enumOf(parameter, values) {
        return `Whoops, the "${parameter}" parameter must be as ${values} !`
    }

    /**
     * For: !tag.length or document.querySelector(tag)
     * 
     * @param {string} tag
     * @returns {string}
     * @returns {string}
     */
    static exist(tag) {
        return `Whoops, there is no <${tag}> !`
    }

    /**
     * For: !document.getElementById(id)
     * 
     * @param {string} tag 
     * @param {string} id
     * @returns {string}
     */
    static existById(tag, id) {
        return `Whoops, there is no <${tag}> with the id "#${id}" !`
    }

    /**
     * For: !tag.hasAttribute(attribute)
     * 
     * @param {string} tag 
     * @param {string} attribute
     * @returns {string}
     */
    static withAttribute(tag, attribute) {
        return `Whoops, the <${tag}> must have the attribute [${attribute}] !`
    }

}