/**
 * ------------------------------------------------------------------
 * Form helper
 * ------------------------------------------------------------------
 * This class regroup some helper function for forms
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 */

import ErrorMessage from "./_error"

export default class FormHelper {

    /**
     * Toggle the visibility of a password input
     * 
     * @param {HTMLElement} button - A <button> element
     */
    static togglePassword(button) {

        // Check for errors
        if (!(button instanceof HTMLElement)) throw new Error(ErrorMessage.instanceOf('button', 'HTMLElement'))
        if (!button.hasAttribute('aria-controls')) throw new Error(ErrorMessage.withAttribute('button', 'aria-controls'))

        // Define the <input>
        const input = document.getElementById(button.getAttribute('aria-controls'))

        // If no <input> return error
        if (!input) throw new Error(ErrorMessage.existById('input', button.getAttribute('aria-controls')))

        // Define the new [type] of the <input>
        const type = input.type === 'password' ? 'text' : 'password'
        input.setAttribute('type', type)

        // Change the [aria-pressed] attribute on the <button>
        button.setAttribute('aria-pressed', type !== 'password')

    }

    /**
     * Toggle the attributes value, disabled and required on multiple fields when is visible
     * 
     * @param {Object} fields - Array of fields to toggle
     * @param {boolean} isVisible - Are the fields visible
     * @param {Object} customOptions - Object of options
     */
    static toggleAttributes(fields, isVisible, customOptions = {}) {

        // Check for errors
        if (!(fields instanceof Object)) throw new Error(ErrorMessage.instanceOf('fields', 'Object'))
        if (typeof isVisible !== 'boolean') throw new Error(ErrorMessage.typeOf('isVisible', 'boolean'))
        if (!(customOptions instanceof Object)) throw new Error(ErrorMessage.instanceOf('customOptions', 'Object'))
        if (customOptions.reset && !['boolean', 'object'].includes(typeof customOptions.reset)) throw new Error(ErrorMessage.typeOf('customOptions.reset', 'boolean|object'))
        if (customOptions.disabled && !['boolean', 'object'].includes(typeof customOptions.disabled)) throw new Error(ErrorMessage.typeOf('customOptions.disabled', 'boolean|object'))
        if (customOptions.required && !['boolean', 'object'].includes(typeof customOptions.required)) throw new Error(ErrorMessage.typeOf('customOptions.required', 'boolean|object'))
        if (customOptions.unchanged && typeof customOptions.unchanged !== 'object') throw new Error(ErrorMessage.typeOf('customOptions.unchanged', 'object'))

        // Set default options
        const defaultOption = {
            reset: true,        // Can be true/false/[name]
            disabled: true,     // Can be true/false/[name]
            required: false,    // Can be true/false/[name]
            unchanged: []       // Array of names 
        }

        // Merge the options
        const options = { ...defaultOption, ...customOptions }

        // Do stuff for each field
        fields.forEach(field => {

            // Get the name
            const name = field.getAttribute('name')

            // Stop if the field must not change
            if (options.unchanged.includes(name)) return

            // Get the needs
            let needReset = typeof options.reset === 'boolean' ? options.reset : options.reset.includes(name)
            let needDisabled = typeof options.disabled === 'boolean' ? options.disabled : options.disabled.includes(name)
            let needRequired = typeof options.required === 'boolean' ? options.required : options.required.includes(name)

            // Reset the field
            if (needReset && !isVisible) {
                field.value = null
                field.checked = null
            }

            // Disabled field
            if (needDisabled) field.disabled = !isVisible

            // Required field
            if (needRequired) field.required = isVisible

        })

    }

}