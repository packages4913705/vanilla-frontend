/**
 * ------------------------------------------------------------------
 * Tabpanel
 * ------------------------------------------------------------------
 * This class enable the functionality to toggles multiple tabpanel
 * 
 * @author Natacha Herth
 * @copyright Natacha Herth, design & web development
 * 
 */

import BaseComponent from './utilities/_base-component'
import ErrorMessage from "./utilities/_error"

export default class Tabpanel extends BaseComponent {

    /**
     * Creates an instance
     *
     * @param {HTMLElement} el - The HTML element
     * @param {object} options - The custom options
     * @constructor
     */
    constructor(el, options = {}) {

        // Run the SUPER constructor from BaseComponent
        super(el, options, 'tabpanel')

        // Check for errors
        if (!el.querySelector('[role="tab"]')) throw new Error(ErrorMessage.exist('button [role="tab"]'))
        if (!el.querySelector('[role="tabpanel"]')) throw new Error(ErrorMessage.exist('div [role="tabpanel"]'))

        // Define the properties
        this._tabs = this._element.querySelectorAll('[role="tab"]')
        this._panels = this._element.querySelectorAll('[role="tabpanel"]')
        this._current = null

        // Init the event listener
        this.#init()

    }

    /**
     * Init the event listener
     * 
     * @private
     */
    #init() {

        // Init first
        if (!this._current) this.#change(this._element.querySelector('[role="tab"][aria-selected="true"]') ?? this._tabs[0])

        // CLICK tabs
        this._tabs.forEach(tab => tab.addEventListener('click', () => this.#change(tab)))

    }

    /**
     * Change the tabpanel
     * @param {HTMLElement} button - A <button> element
     * 
     * @private
     */
    #change(button) {

        // Stop here if the tab is already the current one
        if (button === this._current) return

        // Get the [aria-controls] atribute
        const arialist = button.hasAttribute('aria-controls') ? button.getAttribute('aria-controls') : null
        if (!arialist) return

        // Change the current current
        this._current = button

        // Change the [aria-selected] attribute on tabs
        this._tabs.forEach((tab) => tab.setAttribute('aria-selected', tab === this._current))

        // Change the [hidden] attribute on tabpanels
        this._panels.forEach((panel) => panel.hidden = panel.id !== arialist)

        // Run event after changed
        this.emmitEvent('changed', { current: this._current })

    }

}