# Vanilla Frontend

A simple and minimal framework that prioritize semantic HTML and accessibility.

Quickly start a new project with a clean design and a minimal override for customization.

More info on the(https://vanilla-frontend-packages4913705-8c38a44c52586aedf08b2939a0529c.gitlab.io/)[DOC]

## Bug or suggestion

Please report any bug of issues on (https://gitlab.com/packages4913705/vanilla-frontend/-/issues)[Gitlab] or send me an email !