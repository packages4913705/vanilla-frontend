import { defineConfig } from 'vite'
import glob from 'fast-glob'
import path from 'node:path'
import { fileURLToPath } from 'node:url'

export default defineConfig({
    build: {
        target: 'esnext', //browsers can handle the latest ES features
        cssCodeSplit: false,
        rollupOptions: {
            input: Object.fromEntries(
                glob.sync(['./*.html', './pages/**/*.html']).map(file => [
                    // This remove `pages/` as well as the file extension from each
                    // file, so e.g. pages/nested/foo.html becomes nested/foo
                    path.relative(__dirname, file.slice(0, file.length - path.extname(file).length)),
                    // This expands the relative paths to absolute paths, so e.g.
                    // src/nested/foo becomes /project/src/nested/foo.js
                    fileURLToPath(new URL(file, import.meta.url)),
                ]),
            ),
        },
    }
})