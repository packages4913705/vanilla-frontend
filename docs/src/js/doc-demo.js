const template = document.createElement('template')
template.innerHTML = `
    <div><slot></slot></div>
`

class DocDemo extends HTMLElement {
    constructor() {
        super()
        const shadow = this.attachShadow({ mode: 'open' })
        shadow.append(template.content.cloneNode(true))
    }
}

customElements.define('doc-demo', DocDemo)