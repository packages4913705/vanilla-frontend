class DocLayout extends HTMLElement {
    constructor() {
        super()
        this.innerHTML = `
            <div id="backdrop"></div>
            <header>
                <button class="drawer-button" aria-expanded="false" aria-pressed="false" aria-controls="sidebar" aria-label="Toggle the sidebar navigation">
                    <svg viewbox="0 0 100 100" width="100%">
                        <rect width="100" height="10" x="0" y="10" rx="0"></rect>
                        <rect width="100" height="10" x="0" y="45" rx="0"></rect>
                        <rect width="100" height="10" x="0" y="80" rx="0"></rect>
                    </svg>
                </button>
                <nav aria-label="Mainbar navigation">
                    <ul>
                        <li>
                            <span class="badge">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-angle" viewBox="0 0 16 16">
                                <path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a6 6 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707s.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a6 6 0 0 1 1.013.16l3.134-3.133a3 3 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146m.122 2.112v-.002zm0-.002v.002a.5.5 0 0 1-.122.51L6.293 6.878a.5.5 0 0 1-.511.12H5.78l-.014-.004a5 5 0 0 0-.288-.076 5 5 0 0 0-.765-.116c-.422-.028-.836.008-1.175.15l5.51 5.509c.141-.34.177-.753.149-1.175a5 5 0 0 0-.192-1.054l-.004-.013v-.001a.5.5 0 0 1 .12-.512l3.536-3.535a.5.5 0 0 1 .532-.115l.096.022c.087.017.208.034.344.034q.172.002.343-.04L9.927 2.028q-.042.172-.04.343a1.8 1.8 0 0 0 .062.46z"/>
                                </svg>
                                0.1.21
                            </span>
                        </li>
                        <li>
                            <a href="https://gitlab.com/packages4913705/vanilla-frontend" aria-label="Go to Gitlab">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gitlab" viewBox="0 0 16 16">
                                <path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.npmjs.com/package/@natachah/vanilla-frontend" aria-label="Go to NPMJS">
                                <svg viewBox="0 0 27.23 27.23" aria-hidden="true"><rect fill="currentColor" width="27.23" height="27.23" rx="2"></rect><polygon fill="#fff" points="5.8 21.75 13.66 21.75 13.67 9.98 17.59 9.98 17.58 21.76 21.51 21.76 21.52 6.06 5.82 6.04 5.8 21.75"></polygon></svg>
                            </a>
                        </li>
                    </ul>
                </nav>
            </header>
            <aside id="sidebar" class="drawer" hidden>
                <header>
                    <a href="/index.html">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-rocket" viewBox="0 0 16 16">
                        <path d="M8 8c.828 0 1.5-.895 1.5-2S8.828 4 8 4s-1.5.895-1.5 2S7.172 8 8 8"/>
                        <path d="M11.953 8.81c-.195-3.388-.968-5.507-1.777-6.819C9.707 1.233 9.23.751 8.857.454a3.5 3.5 0 0 0-.463-.315A2 2 0 0 0 8.25.064.55.55 0 0 0 8 0a.55.55 0 0 0-.266.073 2 2 0 0 0-.142.08 4 4 0 0 0-.459.33c-.37.308-.844.803-1.31 1.57-.805 1.322-1.577 3.433-1.774 6.756l-1.497 1.826-.004.005A2.5 2.5 0 0 0 2 12.202V15.5a.5.5 0 0 0 .9.3l1.125-1.5c.166-.222.42-.4.752-.57.214-.108.414-.192.625-.281l.198-.084c.7.428 1.55.635 2.4.635s1.7-.207 2.4-.635q.1.044.196.083c.213.09.413.174.627.282.332.17.586.348.752.57l1.125 1.5a.5.5 0 0 0 .9-.3v-3.298a2.5 2.5 0 0 0-.548-1.562zM12 10.445v.055c0 .866-.284 1.585-.75 2.14.146.064.292.13.425.199.39.197.8.46 1.1.86L13 14v-1.798a1.5 1.5 0 0 0-.327-.935zM4.75 12.64C4.284 12.085 4 11.366 4 10.5v-.054l-.673.82a1.5 1.5 0 0 0-.327.936V14l.225-.3c.3-.4.71-.664 1.1-.861.133-.068.279-.135.425-.199M8.009 1.073q.096.06.226.163c.284.226.683.621 1.09 1.28C10.137 3.836 11 6.237 11 10.5c0 .858-.374 1.48-.943 1.893C9.517 12.786 8.781 13 8 13s-1.517-.214-2.057-.607C5.373 11.979 5 11.358 5 10.5c0-4.182.86-6.586 1.677-7.928.409-.67.81-1.082 1.096-1.32q.136-.113.236-.18Z"/>
                        <path d="M9.479 14.361c-.48.093-.98.139-1.479.139s-.999-.046-1.479-.139L7.6 15.8a.5.5 0 0 0 .8 0z"/>
                        </svg>
                        Vanilla Frontend
                    </a>
                </header>
                <nav aria-label="Sidebar navigation">
                    <h2>Quick start</h2>
                    <ul>
                        <li><a href="/pages/quick-start/installation.html">Installation</a></li>
                        <li><a href="/pages/quick-start/customization.html">Customization</a></li>
                        <li><a href="/pages/quick-start/mixins.html">SCSS Mixins</a></li>
                        <li><a href="/pages/quick-start/conventions.html">Conventions</a></li>
                    </ul>
                    <h2>Base</h2>
                    <ul>
                        <li><a href="/pages/base/reset.html">Reset</a></li>
                        <li><a href="/pages/base/typography.html">Typography</a></li>
                        <li><a href="/pages/base/media.html">Media</a></li>
                    </ul>
                    <h2>Components</h2>
                    <ul>
                        <li><a href="/pages/components/badge.html">Badge</a></li>
                        <li><a href="/pages/components/breadcrumb.html">Breadcrumb</a></li>
                        <li><a href="/pages/components/button.html">Button</a></li>
                        <li><a href="/pages/components/card.html">Card</a></li>
                        <li><a href="/pages/components/dialog.html">Dialog</a></li>
                        <li><a href="/pages/components/disclosure.html">Disclosure</a></li>
                        <li><a href="/pages/components/drawer.html">Drawer</a></li>
                        <li><a href="/pages/components/dropdown.html">Dropdown</a></li>
                        <li><a href="/pages/components/form.html">Form</a></li>
                        <li><a href="/pages/components/grid.html">Grid</a></li>
                        <li><a href="/pages/components/list.html">List</a></li>
                        <li><a href="/pages/components/loading.html">Loading</a></li>
                        <li><a href="/pages/components/progress.html">Progress</a></li>
                        <li><a href="/pages/components/slider.html">Slider</a></li>
                        <li><a href="/pages/components/table.html">Table</a></li>
                    </ul>
                    <h2>Javascript</h2>
                    <ul>
                        <li><a href="/pages/javascript/autofill.html">Autofill</a></li>
                        <li><a href="/pages/javascript/checkall.html">Check all</a></li>
                        <li><a href="/pages/javascript/comfort.html">Comfort</a></li>
                        <li><a href="/pages/javascript/consent.html">Consent</a></li>
                        <li><a href="/pages/javascript/cookie.html">Cookie</a></li>
                        <li><a href="/pages/javascript/form.html">Form helper</a></li>
                        <li><a href="/pages/javascript/scroll.html">Scroll</a></li>
                        <li><a href="/pages/javascript/sortable.html">Sortable</a></li>
                        <li><a href="/pages/javascript/tabpanel.html">Tabpanel</a></li>
                        <li><a href="/pages/javascript/toggle.html">Toggle</a></li>
                        <li><a href="/pages/javascript/tree.html">Tree</a></li>
                    </ul>
                    <div id="copyright">
                        Released under the MIT License.<br>
                        Copyright © 2024-present <a href="https://natachaherth.ch">Natacha Herth</a>
                    </div>
                </nav>
            </aside>
            <main>
            ${this.innerHTML}
            </main>
        `
    }
}

customElements.define('doc-layout', DocLayout)