/**
 * ------------------------------------------------------------------
 * Demo
 * ------------------------------------------------------------------
 * This is the code for the demonstration code
 * 
 * @author Natacha Herth

 * @copyright Natacha Herth, design & web development
 * 
 */

import Dialog from './../../../js/_dialog.js'
const dialogs = document.querySelectorAll('.demo-dialog')
if (dialogs) dialogs.forEach(dialog => new Dialog(dialog))

const demoDialogFormJS = document.getElementById('demoDialogFormJs')
if (demoDialogFormJS) {
    demoDialogFormJS.addEventListener('dialog:submited', (e) => {
        const value = e.detail.form.querySelector('input').value
        document.getElementById('favorite').innerText = value
    })
}

import Dropdown from "./../../../js/_dropdown.js"
const dropdowns = document.querySelectorAll('.demo-dropdown')
if (dropdowns) dropdowns.forEach(dropdown => new Dropdown(dropdown))

import Slider from './../../../js/_slider.js'
//if (sliders) sliders.forEach(slider => new Slider(slider))
const sliderFull = document.getElementById('sliderFull')
if (sliderFull) new Slider(sliderFull, {
    //behavior: 'instant',
    loop: true,
    //autoplay: 1500
})

const sliderFade = document.getElementById('sliderFade')
if (sliderFade) {
    const mySliderObj = new Slider(sliderFade, {
        behavior: 'instant',
        loop: true,
        autoplay: 4000
    })
    sliderFade.addEventListener('slider:changed', (ev) => {
        const src = mySliderObj._slides[ev.detail.previous].querySelector('img').getAttribute('src')
        sliderFade.style.backgroundImage = `url("${src}")`
    })
}

import Autofill from "./../../../js/_autofill"
const autofills = document.querySelectorAll('.demo-autofill')
if (autofills) autofills.forEach(autofill => new Autofill(autofill))

import CheckAll from "./../../../js/_check-all"
const checkboxAll = document.getElementById('demoCheckboxAll')
if (checkboxAll) new CheckAll(checkboxAll)

import Comfort from "./../../../js/_comfort"
new Comfort('demo-confort')

import Consent from "./../../../js/_consent"
if (document.getElementById('demoCookies')) {
    new Consent('demo-consent', 'demoCookies', 'demoCookiesPreferences')
}

import FormHelper from "./../../../js/utilities/_form-helper"
const passwordButton = document.getElementById('demoPassword')
if (passwordButton) passwordButton.addEventListener('click', () => FormHelper.togglePassword(passwordButton))
const demoToggleFormButton = document.getElementById('demoToggleFormButton')
const demoToggleFormContent = document.getElementById('demoToggleFormContent')

if (demoToggleFormButton && demoToggleFormContent) {
    const fields = demoToggleFormContent.querySelectorAll('input,select,textarea')
    new Toggle(demoToggleFormButton)
    demoToggleFormButton.addEventListener('toggle:changed', (e) => {
        FormHelper.toggleAttributes(fields, !demoToggleFormContent.hidden, {
            reset: true,
            disabled: true,
            required: ['test'],
            unchanged: ['test']
        })
    })
}

import Scroll from "./../../../js/_scroll"
const scrollSpy = document.getElementById('demoScrollSpy')
if (scrollSpy) {
    new Scroll(scrollSpy, {
        navigation: 'demoScrollNav'
    })
}

import Sortable from "./../../../js/_sortable"
const sortables = document.querySelectorAll('.demo-sortable')
if (sortables) sortables.forEach(sortable => new Sortable(sortable))

import Toggle from "./../../../js/_toggle.js"
const toggles = document.querySelectorAll('.demo-toggle')
if (toggles) toggles.forEach(toggle => new Toggle(toggle))

import Tree from "./../../../js/_tree"
const trees = document.querySelectorAll('.demo-tree')
if (trees) trees.forEach(tree => new Tree(tree))