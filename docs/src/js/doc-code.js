const template = document.createElement('template')
template.innerHTML = `
    <style>
        pre {
            white-space: preserve;
            word-spacing: normal;
            word-break: normal;
            word-wrap:normal;
            tab-size:0;
            hyphen:none;
            margin: 0;
        }
        .copy-btn {
            all: unset;
            color: white;
            position: absolute;
            top: 0;
            right: 0;
            padding:.75rem;
            font-size:12px;
            line-height:12px;
            border-radius: .5rem;
            opacity: .5;
            transition: opacity .25s ;
        }
        .copy-btn:hover {
            cursor:pointer;
            opacity: 1;
            
        }
        pre {
            overflow: auto;
        }
    </style>
    <button class="copy-btn" aria-label="Copy to clipboard" title="Copy to clipboard">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard" viewBox="0 0 16 16">
            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1z"/>
            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0z"/>
        </svg>
    </button>
    <div class="my-code"></div>
`

import { getHighlighter } from 'shiki'
const highlighter = await getHighlighter({
    themes: ['one-light', 'one-dark-pro'],
    langs: ['javascript', 'html', 'css', 'scss', 'sh'],
})

class DocCode extends HTMLElement {
    constructor() {

        super()

        // Define the shadow
        const shadow = this.attachShadow({ mode: 'open' })
        shadow.append(template.content.cloneNode(true))
        this.code = shadow.querySelector('.my-code')

        // Get the type of code
        const type = this.getAttribute('data-type') ?? 'html'

        // Clean the code
        let content = type === 'html' ? this.innerHTML : this.textContent
        var pattern = content.match(/\s*\n[\t\s]*/)
        content = content.replace(new RegExp(pattern, "g"), '\n').replaceAll('=""', '')
        content = content.trim()

        // Run the shiki plugin
        const shiki = highlighter.codeToHtml(content, {
            lang: type,
            theme: 'one-dark-pro'
        })

        // Replace the content by the shiki <pre>
        this.code.innerHTML = shiki

        // Init copy button
        const delay = (s) => new Promise((resolve) => setTimeout(resolve, s * 1000))
        const copyBtn = shadow.querySelector('.copy-btn')
        if (copyBtn) copyBtn.addEventListener('click', async () => {
            navigator.clipboard.writeText(content)
            copyBtn.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-check" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0"/>
                <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1z"/>
                <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0z"/>
            </svg>
            `
            await delay(2)
            copyBtn.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard" viewBox="0 0 16 16">
                <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1z"/>
                <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0z"/>
            </svg>
            `
        })
    }
}

customElements.define('doc-code', DocCode)

