// Import main css
import './../scss/vanilla-frontend.scss'

// HERE go the code
import './src/js/doc-layout.js'
import './src/js/doc-demo.js'
import './src/js/doc-code.js'
import './src/js/demo.js'

import './src/scss/layout.scss'
import './src/scss/style.scss'
import './src/scss/demo.scss'

// Prevent first animation
setTimeout(() => { document.body.removeAttribute('data-preload') }, 10)

// Set the current navigation
const current = window.location.pathname

if (current.startsWith('/pages/')) document.querySelectorAll(`#sidebar a[href*="${current}"]`).forEach(elem => {
    elem.setAttribute('aria-current', 'page')
})

// My awsome sidebar !!!
import Drawer from './../js/_drawer.js'
const sidebar = document.getElementById('sidebar')
if (sidebar) new Drawer(sidebar, {
    cookie: 'demo-confort'
})

// Code group
import Tabpanel from './../js/_tabpanel.js'
const tabpanels = document.querySelectorAll('.code-group')
if (tabpanels) tabpanels.forEach(tabpanel => new Tabpanel(tabpanel))
