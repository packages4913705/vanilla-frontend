////
/// ------------------------------------------------------------------
/// Mixin
/// ------------------------------------------------------------------
/// Regroup all mixins of the package
///
/// @group abstracts
/// @author Natacha Herth
/// 
////

@use "sass:map";
@use "sass:meta";
@use "sass:list";
@use "./default";
@use "./options" as *;

/// Create a basic item that can be interactive or not
///
/// @param {string} $name - Name of the component
/// @param {array} $states - List of interactions (can be: focus, hover, active, disabled)
/// @param {array} $properties - List of default properties (check in /variables/_setting.scss)
/// @access public
///
@mixin as-item($name, $states: (), $properties: default.$item-properties) {

    // Define the default variables
    // INFO: The choice of SASS variables other CSS private properties if for better readability of the final code
    $default-color: var(--#{$name}-color, map.get($properties, color));
    $default-background: var(--#{$name}-background, map.get($properties, background));
    $default-border-size: var(--#{$name}-border-size, map.get($properties, border-size));
    $default-border-style: var(--#{$name}-border-style, map.get($properties, border-style));
    $default-border-color: var(--#{$name}-border-color, map.get($properties, border-color));
    $default-border-radius: var(--#{$name}-border-radius, map.get($properties, border-radius));
    $default-padding-inline: var(--#{$name}-padding-inline, map.get($properties, padding-inline));
    $default-padding-block: var(--#{$name}-padding-block, map.get($properties, padding-block));

    $default-hover-background: color-mix(in srgb, $default-background, var(--hover-color) var(--hover-percent));
    $default-active-background: color-mix(in srgb, $default-background, var(--active-color) var(--active-percent));

    // Define the CSS
    color: $default-color;
    background-color: $default-background;
    border: $default-border-size $default-border-style $default-border-color;
    border-radius: $default-border-radius;
    padding: $default-padding-block $default-padding-inline;

    // Add cursor, text decoration and transition if needed
    // ! Must be before any custom things SASS and CSS new convention 
    @if($states !=()) {
        cursor: var(--#{$name}-cursor, pointer);
        text-decoration: var(--#{$name}-decoration, var(--decoration));

        @media screen and (prefers-reduced-motion: no-preference) {
            transition: var(--#{$name}-transition, all .25s ease-in-out);
        }
    }

    // Define the outline variations
    @include with-outline-variations($name, $states);

    // Define the interactive states
    @if($states !=()) {

        // State: hover
        @if(list.index($states, 'hover')) {
            &:hover {
                color: var(--#{$name}-hover-color, $default-color);
                background-color: var(--#{$name}-hover-background, $default-hover-background);
                border-color: var(--#{$name}-hover-border-color, $default-border-color);
            }
        }

        // State: focus
        @if(list.index($states, 'focus')) {

            &:focus,
            &:focus-visible {
                color: var(--#{$name}-focus-color, var(--#{$name}-hover-color, $default-color));
                background-color: var(--#{$name}-focus-background, var(--#{$name}-hover-background, $default-hover-background));
                border-color: var(--#{$name}-focus-border-color, var(--#{$name}-hover-border-color, $default-border-color));
                outline: var(--#{$name}-outline-size, var(--outline-size)) var(--#{$name}-outline-style, var(--outline-style)) var(--#{$name}-outline-color, color-mix(in srgb, var(--#{$name}-border-color, currentcolor), transparent var(--outline-opacity)));
                outline-offset: var(--#{$name}-outline-offset, var(--outline-offset));
            }
        }

        // State: active
        @if(list.index($states, 'active')) {

            &:active,
            &[aria-current],
            &[aria-selected=true],
            &[aria-pressed=true] {
                color: var(--#{$name}-active-color, $default-color);
                background-color: var(--#{$name}-active-background, $default-active-background);
                border-color: var(--#{$name}-active-border-color, $default-border-color);
            }
        }

        // State: disabled
        @if(list.index($states, 'disabled')) {
            &:disabled {
                pointer-events: none;
                opacity: var(--#{$name}-disabled-opacity, var(--disabled-opacity));
            }
        }

    }

    // Define the color variations
    @include with-color-variations($name, $states);

}

/// Create a basic list group that can be interactive
///
/// @require {mixin} as-item
/// @param {string} $name - Name of the component
/// @param {array} $states - List of interactions (can be: focus, hover, active, disabled)
/// @param {array} $properties - List of default properties (check in /variables/_setting.scss)
/// @access public
///
@mixin as-list($name, $states: (), $properties: default.$item-properties) {

    // Reset
    margin: 0;
    padding: 0;
    border-radius: var(--#{$name}-border-radius, map.get($properties, border-radius));
    background-color: var(--#{$name}-background, map.get($properties, background));

    &:is(ul, ol) {
        list-style: none;
    }

    > * {
        position: relative;
        margin: 0;
        text-align: var(--list-text-align, left);

        &:has(> a[role=button]:only-child, > button:only-child) > * {
            display: block;
            width: 100%;
            text-align: var(--list-text-align, left);
            @include as-item($name, $states, $properties);
        }

        &:not(:has(> a[role=button]:only-child, > button:only-child)) {
            @include as-item($name, (), $properties);
        }

        & + *,
        & + * > :only-child {
            border-top: none !important;
        }

        // Remove radius on middle child
        &:not(:first-child, :last-child),
        &:not(:first-child, :last-child) > :only-child {
            border-radius: 0 !important;
        }

        // Remove radius on first/last child
        &:not(:only-child) {

            &:first-child,
            &:first-child > :only-child {
                border-end-start-radius: 0 !important;
                border-end-end-radius: 0 !important;
            }

            &:last-child,
            &:last-child > :only-child {
                border-start-start-radius: 0 !important;
                border-start-end-radius: 0 !important;
            }

        }

    }

}

/// Create a basic item with an header and a footer style
///
/// @param {string} $name - Name of the component
/// @param {array} $properties - List of default properties (check in /variables/_setting.scss)
/// @access public
///
@mixin with-header-and-footer($name, $properties: default.$item-properties) {

    > header,
    > footer {
        margin-inline: calc(var(--#{$name}-padding-inline, map.get($properties, padding-inline)) * -1);
        padding: calc(var(--#{$name}-padding-block, map.get($properties, padding-block)) / 2) var(--#{$name}-padding-inline, map.get($properties, padding-inline));
    }

    > header {
        margin-top: calc(var(--#{$name}-padding-block, map.get($properties, padding-block)) * -1);
        border-start-start-radius: inherit;
        border-start-end-radius: inherit;
        border-bottom: var(--#{$name}-divider-size, var(--#{$name}-border-size, var(--border-size))) var(--#{$name}-divider-style, var(--#{$name}-border-style, var(--border-style))) var(--#{$name}-divider-color, var(--#{$name}-border-color, transparent));
    }

    > footer {
        margin-bottom: calc(var(--#{$name}-padding-block, map.get($properties, padding-block)) * -1);
        border-end-start-radius: inherit;
        border-end-end-radius: inherit;
        border-top: var(--#{$name}-divider-size, var(--#{$name}-border-size, var(--border-size))) var(--#{$name}-divider-style, var(--#{$name}-border-style, var(--border-style))) var(--#{$name}-divider-color, var(--#{$name}-border-color, transparent));
    }

}

/// Create some outline variations
///
/// @require {variable} $outline-variations - Map variable
/// @param {string} $name - Name of the component
/// @param {array} $states - List of interactions
/// @param {string} $color - The initial color of the item (used for color, border and :hover background)
/// @param {string} $contrast - The initial contrast color of the item (:hover color)
/// @access public
///
@mixin with-outline-variations($name, $states: (), $color: var(--#{$name}-background, var(--color-font)), $contrast: var(--#{$name}-color, var(--color-body))) {

    @if(meta.global-variable-exists('outline-variations') and list.index($outline-variations, $name)) {

        &.outline {

            color: $color;
            background-color: transparent;
            border-color: currentColor;

            @if(list.index($states, 'hover')) {

                &:hover {
                    color: $contrast;
                    background-color: $color;
                    border-color: $color;
                }

            }

            @if(list.index($states, 'active')) {

                &:active,
                &[aria-current],
                &[aria-pressed=true] {
                    color: $contrast;
                    background-color: $color;
                    border-color: $color;
                }

            }

        }

    }

}

/// Create some color variations
///
/// @require {variable} color-variations - Map variable
/// @param {string} $name - Name of the component
/// @param {boolean} @param {array} $states - List of interactions
/// @access public
///
@mixin with-color-variations($name, $states: ()) {

    @if(meta.global-variable-exists('color-variations') and map.get($color-variations, #{$name})) {

        @each $colorname in map.get($color-variations, #{$name}) {

            &.#{$colorname} {

                // Define the variables
                $color: var(--color-#{$colorname});
                $contrast: var(--color-#{$colorname}-contrast, white);

                // Define the custom properties
                --#{$name}-color: #{$contrast};
                --#{$name}-background: #{$color};
                --#{$name}-border-color: #{$color};

                @if(list.index($states, 'hover')) {

                    // Define the variables
                    $hover: color-mix(in srgb, $color, var(--hover-color) var(--hover-percent));

                    // Define the custom properties
                    --#{$name}-hover-color: #{$contrast};
                    --#{$name}-hover-background: #{$hover};
                    --#{$name}-hover-border-color: #{$hover};

                }

                @if(list.index($states, 'active')) {

                    // Define the variables
                    $active: color-mix(in srgb, $color, var(--active-color) var(--active-percent));

                    // Define the custom properties
                    --#{$name}-active-color: #{$contrast};
                    --#{$name}-active-background: #{$active};
                    --#{$name}-active-border-color: #{$active};

                }

            }

        }

    }

}

/// Create a responsive table
///
/// @access public
///
@mixin as-responsive-table() {

    // Remove the header & the border
    thead {
        display: none;
    }

    // Remove the header border
    tbody tr:first-child {
        border-top: none;
    }

    // Display as a grid
    th,
    td {
        display: grid;
        grid-template-columns: var(--table-grid-template, 15ch 1fr);

        &[data-label] {
            &::before {
                padding-right: var(--table-padding-inline, var(--padding-inline));
                content: attr(data-label);
            }
        }
    }

}

/// Offset a flex grid column 
///
/// @param {int} $number - Number of column to offset
/// @access public
///
@mixin flex-grid-offset-column($number: 1) {
    margin-left: calc(($number * 100%) / var(--grid-columns, 12))
}

/// Make a flex grid column wider
///
/// @param {int} $number - Number of column width
/// @access public
///
@mixin flex-grid-wider-column($number: 2) {
    flex-basis: calc($number / var(--grid-columns, 12) * 100% - var(--grid-gap-inline, 1rem) * (var(--grid-columns, 12) - 1) / var(--grid-columns, 12))
}